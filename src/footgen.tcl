#!/opt/ActiveTcl-8.6/bin/tclsh
# Hey Emacs, use -*- Tcl -*- mode

set scriptname [file rootname $argv0]

set thisfile [file normalize [info script]]

# Directory where this script lives
set program_directory [file dirname $thisfile]

# Directory from which the script was invoked
set invoked_directory [pwd]

######################## Command line parsing ########################

package require cmdline
set options {
    {o.arg "footprint.fp" "Output filename"}
    {t.arg "db"        "Footprint type"}
    {t?                   "List known footprint types"}
    {n.arg "8"            "Number of pins"}
    {ps.arg "0.25"        "Separation to copper pours / polygons (mm)"}
    {cs.arg "0.5"         "Courtyard separation or excess (mm)"}
    {ms.arg "0.076"       "Separation to solder mask (mm)"}
    {lt.arg "0.127"       "Element silkscreen line thickness (mm)"}
    {d.arg  "1.0"         "Dimension (mm) -- used for single-pad footprints"}
}

set usage "usage: $scriptname \[options\]"

# Process command line parameters
try {
    array set params [::cmdline::getoptions argv $options $usage]
} trap {CMDLINE USAGE} {message optdict} {
    # message will be the human-readable error.  optdict will be the
    # options dictionary present when the command was attempted.
    puts $message
    exit 1
}



if {[string match "0603?" $params(t)] || \
	[string match "1210?" $params(t)] || \
	[string match "apt1608" $params(t)]} {
    # This is a 2-pin chip part
    set params(n) 2
}

proc iterint {start points} {
    # Return a list of increasing integers starting with start with
    # length points
    set count 0
    set intlist [list]
    while {$count < $points} {
	lappend intlist [expr $start + $count]
	incr count
    }
    return $intlist
}

proc dashline {width} {
    # Return a string of dashes of length width
    set dashline ""
    foreach dashchar [iterint 0 $width] {
	append dashline "-"
    }
    return $dashline
}

# Constants
array set constants {
    pi 3.1415926535897932385
}

proc list_known_footprint_types {} {
    # List the known footprint types
    set footprint_width 20
    set description_width 20
    set format_string "%-*s %-*s"
    set header [format $format_string $footprint_width "Footprint" \
		    $description_width "Description"]
    puts $header
    puts [dashline [string length $header]]
    puts [format $format_string $footprint_width "eple" \
	      $description_width \
	      "PCB-mount USB-A connector -- see Wurth 8492121 datasheet"]
    puts [format $format_string $footprint_width "musbr" \
	      $description_width \
	      "PCB-mount rugged USB-C connector -- see MUSBRM1C130 datasheet"]
    puts [format $format_string $footprint_width "d" \
	      $description_width \
	      "D package from TI, aka SOP -- see application note snoa293b"]
    puts [format $format_string $footprint_width "dda" \
	      $description_width \
	      "DDA package from TI, aka SO -- see TPS2421 (7-61)"]
    puts [format $format_string $footprint_width "db" \
	      $description_width \
	      "DB package from TI, aka SSOP -- see application note snoa293b"]
    puts [format $format_string $footprint_width "dca" \
	      $description_width \
	      "DCA package from TI, aka HTSSOP -- see TAS5753MD datasheet"]
    puts [format $format_string $footprint_width "dgk" \
	      $description_width \
	      "DGK package from TI, aka VSSOP -- see LM75A datasheet"]
    puts [format $format_string $footprint_width "dcu" \
	      $description_width \
	      "DCU package from TI, aka US8 -- see TS5A3153 datasheet"]
    puts [format $format_string $footprint_width "dbz" \
	      $description_width \
	      "DBZ package from TI, aka SOT23-3 -- see REF33xx datasheet"]
    puts [format $format_string $footprint_width "dbv" \
	      $description_width \
	      "DBV package from TI, aka SOT23-6 -- see DBV0006A land pattern"]
    puts [format $format_string $footprint_width "sot323" \
	      $description_width \
	      "SOT323 package from Diodes Inc -- see DMN2058 datasheet"]
    puts [format $format_string $footprint_width "tst" \
	      $description_width \
	      "Samtec TST-series shrouded, keyed, through-hole headers"]
    puts [format $format_string $footprint_width "tstsm" \
	      $description_width \
	      "Samtec HTST-series shrouded, keyed, surface-mount headers"]
    puts [format $format_string $footprint_width "shf" \
	      $description_width \
	      "Samtec SHF-series shrouded, keyed, through-hole 50-mil headers"]
    puts [format $format_string $footprint_width "tsws" \
	      $description_width \
	      "Samtec TSW-series single-row unshrouded through-hole headers"]
    puts [format $format_string $footprint_width "jstph" \
	      $description_width \
	      "JST PH-series single-row shrouded through-hole headers"]
    puts [format $format_string $footprint_width "tswsra" \
	      $description_width \
	      "Samtec TSW-series single-row unshrouded right-angle through-hole headers"]
    puts [format $format_string $footprint_width "mta100" \
	      $description_width \
	      "TE polarized header for MTA-100 plugs"]
    puts [format $format_string $footprint_width "lmzfl" \
	      $description_width \
	      "Weidmuller TMZFL terminal blocks"]
    puts [format $format_string $footprint_width "r78e" \
	      $description_width \
	      "Recom R-78E series DC/DC converters"]
    puts [format $format_string $footprint_width "0603r" \
	      $description_width \
	      "0603 imperial (EIA), 1608 metric chip resistor"]
    puts [format $format_string $footprint_width "1206r" \
	      $description_width \
	      "1206 imperial (EIA), 3216 metric chip resistor"]
    puts [format $format_string $footprint_width "0603c" \
	      $description_width \
	      "0603 imperial (EIA), 1608 metric chip capacitor"]
    puts [format $format_string $footprint_width "1206c" \
	      $description_width \
	      "1206 imperial (EIA), 3216 metric chip capacitor"]
    puts [format $format_string $footprint_width "1210c" \
	      $description_width \
	      "1210 imperial (EIA), 3225 metric chip capacitor"]
    puts [format $format_string $footprint_width "4x4l" \
	      $description_width \
	      "4mm x 4mm inductor -- see Wurth WE-LQS series"]
    puts [format $format_string $footprint_width "8x8l" \
	      $description_width \
	      "8mm x 8mm inductor -- see Wurth WE-LQS series"]
    puts [format $format_string $footprint_width "ucl10x10" \
	      $description_width \
	      "10mm x 10mm UCL-series capacitor from Nichicon"]
    puts [format $format_string $footprint_width "1612b" \
	      $description_width \
	      "1612 imperial ferrite bead -- see Wurth WE-MPSB series"]
    puts [format $format_string $footprint_width "apt1608" \
	      $description_width \
	      "APT1608 flat top chip LEDs from Kingbright (0603)"]
    puts [format $format_string $footprint_width "apt3216" \
	      $description_width \
	      "APT3216 flat top chip LEDs from Kingbright (1206)"]
    puts [format $format_string $footprint_width "smcw8" \
	      $description_width \
	      "WL-SMCW 0805 LEDs from Wurth"]
    puts [format $format_string $footprint_width "0805l" \
	      $description_width \
	      "0805 imperial (EIA), 2012 metric chip inductor"]
    puts [format $format_string $footprint_width "smad" \
	      $description_width \
	      "SMA diode"]
    puts [format $format_string $footprint_width "cts625" \
	      $description_width \
	      "Model 625 oscillators from CTS"]
    puts [format $format_string $footprint_width "dscdfn" \
	      $description_width \
	      "Model DSC60XX oscillators from Microchip in DFN packages"]
    puts [format $format_string $footprint_width "pts645" \
	      $description_width \
	      "PTS645 6mm tactile switches from C&K"]
    puts [format $format_string $footprint_width "squarepad" \
	      $description_width \
	      "Single square pad of dimension d mm with no paste"]
    puts [format $format_string $footprint_width "maskpad" \
	      $description_width \
	      "Single square pad of dimension d mm covered with soldermask"]
    puts [format $format_string $footprint_width "snapspace" \
	      $description_width \
	      "Keystone snap-in stacking spacers"]
    puts [format $format_string $footprint_width "6-32p" \
	      $description_width \
	      "Plated hole for 6-32 screws"]
    puts [format $format_string $footprint_width "m7p" \
	      $description_width \
	      "Plated hole for M7 screws"]
    puts [format $format_string $footprint_width "anano" \
	      $description_width \
	      "Arduino Nano"]
    puts [format $format_string $footprint_width "xover_click" \
	      $description_width \
	      "Crossover click board from Mikroe"]
    puts [format $format_string $footprint_width "mp3_2_click" \
	      $description_width \
	      "MP3 2 click board from Mikroe"]
    puts [format $format_string $footprint_width "audioamp_2_click" \
	      $description_width \
	      "AudioAmp 2 click board from Mikroe"]
    puts [format $format_string $footprint_width "astar_328pb" \
	      $description_width \
	      "A-Star 328PB board from Pololu"]
    puts [format $format_string $footprint_width "smbth" \
	      $description_width \
	      "Through-hole SMB connector"]
    puts [format $format_string $footprint_width "sj5" \
	      $description_width \
	      "CUI SJ5-43502PM 3.5mm panel-mount jack"]
    puts [format $format_string $footprint_width "3386f" \
	      $description_width \
	      "Bourns 3386F PCB-mount potentiometers"]
    puts [format $format_string $footprint_width "ces-571423" \
	      $description_width \
	      "CUI CES-series loudspeaker"]
}

proc deg_to_rad {degrees} {
    # Return the angle in radians
    global constants
    set radians [expr $degrees * ($constants(pi) / 180)]
    return $radians
}

proc rad_to_deg {radians} {
    # Return the angle in degrees
    global constants
    set degrees [expr $radians * (180 / $constants(pi))]
    return $degrees
}

proc is_even {number} {
    if [expr $number % 2] {
	# Number is odd
	return false
    } else {
	# Number is even
	return true
    }
}

proc element_line {line_list} {
    # Return text line for the element definition
    global params
    set refdes_offset_mm 1
    if {[string first "Pad" [lindex $line_list 0]] >= 0} {
	# This is a list of pads
	lassign [pad_outer_extremes $line_list] xmin ymax xmax ymin
    } elseif {[string first "Pin" [lindex $line_list 0]] >= 0} {
	# This is a list of pins
	lassign [pin_outer_extremes $line_list] xmin ymax xmax ymin
    }
    set element_flags "\"\""
    set description "\"Created by footgen\""
    set refdes "\"??\""
    set value "\"none\""
    set x_pos "1000mil"
    set y_pos "1000mil"
    if {[string match "mta100" $params(t)]} {
	set text_x "[expr $xmax + 2.54]mm"
	set text_y "0mm"
    } elseif {[string match "tst" $params(t)]} {
	set text_x "[expr $xmax + 6]mm"
	set text_y "0mm"
    } elseif {[string match "shf" $params(t)]} {
	set text_x "[expr $xmax + 1.5]mm"
	set text_y "-0.75mm"
    } elseif {[string match "snapspace" $params(t)]} {
	set text_x "4.0mm"
	set text_y "-0.75mm"
    } elseif {[string match "tsws" $params(t)]} {
	set text_x "[expr $xmax + 1.5]mm"
	set text_y "-0.75mm"
    } elseif {[string match "pts645" $params(t)]} {
	set text_x "[expr $xmax + 1.5]mm"
	set text_y "-0.75mm"
    } elseif {[string match "dda" $params(t)]} {
	set text_x "[expr $xmax + 1.5]mm"
	set text_y "-0.75mm"
    } elseif {[string match "dbz" $params(t)]} {
	set text_x "[expr $xmax + 1.5]mm"
	set text_y "-0.75mm"
    } elseif {[string match "r78e" $params(t)]} {
	set text_x "[expr $xmax + 4]mm"
	set text_y "0mm"
    } elseif {[string match "1612b" $params(t)]} {
	set text_x "[expr $xmax + 3]mm"
	set text_y "-0.75mm"
    } elseif {[string match "1206?" $params(t)]} {
	set text_x "[expr $xmax + 1.5]mm"
	set text_y "-0.75mm"
    } elseif {[string match "0805l" $params(t)]} {
	set text_x "[expr $xmax + 1.5]mm"
	set text_y "-0.75mm"
    } elseif {[string match "4x4l" $params(t)]} {
	set text_x "[expr $xmax + 2]mm"
	set text_y "-0.75mm"
    } elseif {[string match "8x8l" $params(t)]} {
	set text_x "[expr $xmax + 3]mm"
	set text_y "-1.0mm"
    } elseif {[string match "smad" $params(t)]} {
	set text_x "[expr $xmax + 2]mm"
	set text_y "-0.75mm"
    } elseif {[string match "lmzfl" $params(t)]} {
	set text_x "[expr $xmax + 5]mm"
	set text_y "-0.75mm"
    } elseif {[string match "cts625" $params(t)]} {
	set text_x "[expr $xmax + 1.5]mm"
	set text_y "-0.75mm"
    } elseif {[string match "squarepad" $params(t)]} {
	set text_x "[expr $xmax + 1.5]mm"
	set text_y "-0.75mm"
    } elseif {[string match "ucl10x10" $params(t)]} {
	set text_x "[expr $xmax + 2]mm"
	set text_y "-0.75mm"
    } elseif {[string match "musbr" $params(t)]} {
	set text_x "[expr $xmax + 3]mm"
	set text_y "0mm"
    } else {
	set text_x "[expr $xmax + $refdes_offset_mm]mm"
	set text_y "[expr $ymin]mm"
    }
    # Text direction -- 0 is horizontal
    set text_direction 0
    # Set text size in percentage of the default 40mils
    set text_scale 100
    set text_flags "\"\""
    set textline "Element\[ $element_flags $description $refdes $value "
    append textline "$x_pos $y_pos $text_x $text_y $text_direction $text_scale "
    append textline "$text_flags \]"
    return $textline
}

proc dil_pad_line {pin pins inner outer width pitch} {
    # Return the (surface-mount) pad line for one pin in a dual-inline footprint
    #
    # Arguments:
    #   pin -- Pin number
    #   pins -- Total number of pins
    #   inner -- Inner pad-to-pad spacing (floating point mm)
    #   outer -- Outer pad-to-pad spacing (floating point mm)
    #   width -- pad width (floating point mm)
    #   pitch -- Pin-to-pin spacing (floating point mm)
    global params
    # Calculate pad length
    set length [expr ($outer - $inner)/double(2)]
    if {$length > $width} {
	set trace_width $width
    } else {
	set trace_width $length
    }
    puts "Creating DIL pad $pin of $pins"
    if [is_even [expr $pins/2]] {
	# Even number of pins for each row
	if {$pin <= $pins/2} {
	    # Bottom row
	    if {$length > $width} {
		set pad_x1 [expr double($pitch) * ($pin - $pins/4 - 0.5)]
		set pad_y1 [expr double($inner)/2 + double($width)/2]
		set pad_x2 $pad_x1
		set pad_y2 [expr double($outer)/2 - double($width)/2]
	    } else {
		set pad_y1 [expr double($inner)/2 + double($length)/2]
		set pad_x1 [expr double($pitch) * ($pin - $pins/4 - 0.5) -\
				double($width)/2 + double($trace_width)/2]
		set pad_y2 $pad_y1
		set pad_x2 [expr double($pitch) * ($pin - $pins/4 - 0.5) +\
				double($width)/2 - double($trace_width)/2]
	    }

	} else {
	    # Top row
	    if {$length > $width} {
		set pad_x1 [expr double($pitch) * (3 * $pins/4 - $pin + 0.5)]
		set pad_y1 [expr -double($inner)/2 - double($width)/2]
		set pad_x2 $pad_x1
		set pad_y2 [expr -double($outer)/2 + double($width)/2]
	    } else {
		set pad_y1 [expr -double($inner)/2 - double($length)/2]
		set pad_x1 [expr double($pitch) * (3 * $pins/4 - $pin + 0.5) -\
				double($width)/2 + double($trace_width)/2]
		set pad_y2 $pad_y1
		set pad_x2 [expr double($pitch) * (3 * $pins/4 - $pin + 0.5) +\
				double($width)/2 - double($trace_width)/2]
	    }

	}
    } else {
	# Odd number of pins for each row
	if {$pin <= $pins/2} {
	    # Bottom row
	    if {$length > $width} {
		set pad_x1 [expr double($pitch) * ($pin - 0.25 * $pins - 0.5)]
		set pad_y1 [expr (0.5 * $inner) + (0.5 * $width)]
		set pad_x2 $pad_x1
		set pad_y2 [expr (0.5 * $outer) - (0.5 * $width)]
	    } else {
		set pad_y1 [expr (0.5 * $inner) + (0.5 * $length)]
		set pad_x1 [expr double($pitch) * ($pin - 0.25 * $pins - 0.5) -\
				(0.5 * $width + 0.5 * $trace_width)]
		set pad_y2 $pad_y1
		set pad_x2 [expr double($pitch) * ($pin - 0.25 * $pins - 0.5) +\
				(0.5 * $width - 0.5 * $trace_width)]
	    }

	} else {
	    # Top row
	    if {$length > $width} {
		set pad_x1 [expr double($pitch) * (0.75 * $pins - $pin + 0.5)]
		set pad_y1 [expr -0.5 * $inner - 0.5 * $width]
		set pad_x2 $pad_x1
		set pad_y2 [expr -0.5 * $outer + 0.5 * $width]
	    } else {
		set pad_y1 [expr -0.5 * $inner - 0.5 * $length]
		set pad_x1 [expr double($pitch) * (0.75 * $pins - $pin + 0.5) -\
				0.5 * $width + 0.5 * $trace_width]
		set pad_y2 $pad_y1
		set pad_x2 [expr double($pitch) * (0.75 * $pins - $pin + 0.5) +\
				0.5 * $width - 0.5 * $trace_width]
	    }

	}
    }
    set textline "Pad \[ [format "%0.3fmm %0.3fmm" $pad_x1 $pad_y1] "
    append textline "[format "%0.3fmm %0.3fmm" $pad_x2 $pad_y2] "
    append textline "${trace_width}mm "
    # The copper pour clearance is 2x the actual clearance
    append textline "[format "%0.3fmm" [expr double($params(ps)) * 2]] "
    # The solder mask clearance is the entire opening
    append textline "[format "%0.3fmm" [expr double($params(ms)) * 2 + $trace_width]] "
    append textline "\"$pin\" \"$pin\" \"square\"\]"
    return $textline
}

proc dil_connector_pad_line {pin pins inner outer width pitch} {
    # Return the (surface-mount) pad line for one pin in a dual-inline connector footprint
    #
    # Arguments:
    #   pin -- Pin number
    #   pins -- Total number of pins
    #   inner -- Inner pad-to-pad spacing (floating point mm)
    #   outer -- Outer pad-to-pad spacing (floating point mm)
    #   width -- pad width (floating point mm)
    #   pitch -- Pin-to-pin spacing (floating point mm)
    global params
    # Calculate pad length
    set length [expr ($outer - $inner)/double(2)]

    if [is_even [expr $pins/2]] {
	# Even number of pins for each row
	if [is_even $pin] {
	    # Top row
	    set index [expr $pin/2 - 1]
	    set pad_x1 [expr double($pitch) * ($index - double($pins)/4 + 0.5)]
	    set pad_y1 [expr -double($inner)/2 - double($width)/2]
	    set pad_x2 $pad_x1
	    set pad_y2 [expr -double($outer)/2 + double($width)/2]
	} else {
	    # Bottom row
	    set index [expr ($pin + 1)/2 - 1]
	    set pad_x1 [expr double($pitch) * ($index - double($pins)/4 + 0.5)]
	    set pad_y1 [expr double($inner)/2 + double($width)/2]
	    set pad_x2 $pad_x1
	    set pad_y2 [expr double($outer)/2 - double($width)/2]
	}
    } else {
	# Odd number of pins for each row
	if [is_even $pin] {
	    # Top row
	    set index [expr $pin/2 -1]
	    set pad_x1 [expr double($pitch) * ($index  - double($pins)/4 + 0.5)]
	    set pad_y1 [expr -double($inner)/2 - double($width)/2]
	    set pad_x2 $pad_x1
	    set pad_y2 [expr -double($outer)/2 + double($width)/2]
	} else {
	    # Bottom row
	    set index [expr ($pin + 1)/2 - 1]
	    set pad_x1 [expr double($pitch) * ($index  - double($pins)/4 + 0.5)]
	    set pad_y1 [expr double($inner)/2 + double($width)/2]
	    set pad_x2 $pad_x1
	    set pad_y2 [expr double($outer)/2 - double($width)/2]
	}
    }
    set textline "Pad \[ [format "%0.3fmm %0.3fmm" $pad_x1 $pad_y1] "
    append textline "[format "%0.3fmm %0.3fmm" $pad_x2 $pad_y2] "
    append textline "${width}mm "
    # The copper pour clearance is 2x the actual clearance
    append textline "[format "%0.3fmm" [expr double($params(ps)) * 2]] "
    # The solder mask clearance is the entire opening
    append textline "[format "%0.3fmm" [expr double($params(ms)) * 2 + $width]] "
    append textline "\"$pin\" \"$pin\" \"square\"\]"
    return $textline
}

proc dil_pin_line {pin pins row drill ring pitch} {
    # Return the (through-hole) pin line for one pin in a dual-inline IC (DIP) footprint
    #
    # Arguments:
    #   pin -- Pin number
    #   pins -- Total number of pins
    #   row -- Pin-to-pin spacing between rows of pins
    #   drill -- Drill diameter (floating point mm)
    #   ring -- Width of annular ring (floating point mm)
    #   pitch -- Pin-to-pin spacing (floating point mm)
    global params
    puts "Creating DIL pin $pin of $pins"
    if [is_even [expr $pins/2]] {
	# Even number of pins for each row
	if {$pin <= $pins/2} {
	    # Bottom row
	    set pin_x [expr double($pitch) * ($pin - $pins/4 - 0.5)]
	    set pin_y [expr double($row)/2]
	} else {
	    # Top row
	    set pin_x [expr double($pitch) * (3 * $pins/4 - $pin + 0.5)]
	    set pin_y [expr -double($row)/2]
	}
    } else {
	# Odd number of pins for each row
	if {$pin <= $pins/2} {
	    # Bottom row
	    set pin_x [expr double($pitch) * ($pin - 0.25 * $pins - 0.5)]
	    set pin_y [expr double($row)/2]
	} else {
	    # Top row
	    set pin_x [expr double($pitch) * (-$pin + 0.75 * $pins + 0.5)]
	    set pin_y [expr -double($row)/2]
	}
    }
    # The thickness parameter is the total copper diameter
    set thickness [expr $drill + double($ring) * 2]
    set textline "Pin \[ [format "%0.3fmm %0.3fmm" $pin_x $pin_y] "
    append textline "[format "%0.3fmm" $thickness] "
    # The copper pour clearance is 2x the actual clearance
    append textline "[format "%0.3fmm" [expr double($params(ps)) * 2]] "
    # The solder mask clearance is the entire opening
    append textline "[format "%0.3fmm" [expr double($params(ms)) * 2 + $thickness]] "
    append textline "[format "%0.3fmm" $drill] "
    append textline "\"$pin\" \"$pin\" "
    if [string match 1 $pin] {
	append textline "\"square\""
    } else {
	append textline "\"\""
    }
    append textline "\]"
    return $textline
}

proc dbz_pad_line {pin pins inner outer width pitch} {
    # Return the pad line for one pin in a DBZ (SOT23-3) footprint
    #
    # Arguments:
    #   pin -- Pin number
    #   pins -- Total number of pins
    #   inner -- Inner pad-to-pad spacing (floating point mm)
    #   outer -- Outer pad-to-pad spacing (floating point mm)
    #   width -- pad width (floating point mm)
    #   pitch -- Pin-to-pin spacing (floating point mm)
    global params
    # Calculate pad length
    set length [expr ($outer - $inner)/double(2)]
    if {$length > $width} {
	set trace_width $width
    } else {
	set trace_width $length
    }
    if {$pin <= 2} {
	# Bottom row
	if {$length > $width} {
	    set pad_x1 [expr double($pitch) * (2 * $pin - 3)]
	    set pad_y1 [expr double($inner)/2 + double($width)/2]
	    set pad_x2 $pad_x1
	    set pad_y2 [expr double($outer)/2 - double($width)/2]
	} else {
	    set pad_y1 [expr double($inner)/2 + double($length)/2]
	    set pad_x1 [expr double($pitch) * (2 * $pin - 3) -\
			    double($width)/2 + double($trace_width)/2]
	    set pad_y2 $pad_y1
	    set pad_x2 [expr double($pitch) * (2 * $pin - 3) +\
			    double($width)/2 - double($trace_width)/2]
	}

    } else {
	# Top row
	if {$length > $width} {
	    set pad_x1 0
	    set pad_y1 [expr -double($inner)/2 - double($width)/2]
	    set pad_x2 $pad_x1
	    set pad_y2 [expr -double($outer)/2 + double($width)/2]
	} else {
	    set pad_y1 [expr -double($inner)/2 - double($length)/2]
	    set pad_x1 [expr 0 - double($width)/2 + double($trace_width)/2]
	    set pad_y2 $pad_y1
	    set pad_x2 [expr 0 + double($width)/2 - double($trace_width)/2]

	}

    }
    set textline "Pad \[ [format "%0.3fmm %0.3fmm" $pad_x1 $pad_y1] "
    append textline "[format "%0.3fmm %0.3fmm" $pad_x2 $pad_y2] "
    append textline "${trace_width}mm "
    # The copper pour clearance is 2x the actual clearance
    append textline "[format "%0.3fmm" [expr double($params(ps)) * 2]] "
    # The solder mask clearance is the entire opening
    append textline "[format "%0.3fmm" [expr double($params(ms)) * 2 + $trace_width]] "
    append textline "\"$pin\" \"$pin\" \"square\"\]"
    return $textline
}

proc chip_pad_line {pin gap length width} {
    # Return the pad line for one pin in a 2-pin chip footprint
    #
    # Arguments:
    #   pin -- Pin number (1 or 2)
    #   gap -- Separation between pads (floating point mm)
    #   length -- Pad length along long axis of part (floating point mm)
    #   width -- Pad width along short axis of part (floating point mm)
    global params
    if {$length >= $width} {
	set trace_width $width
	set pad_x1 [expr double($trace_width)/2 - double($gap)/2 - $length]
	set pad_y1 0
	set pad_x2 [expr -double($gap)/2 - double($trace_width)/2]
	set pad_y2 0
    } else {
	set trace_width $length
	set pad_x1 [expr -double($gap)/2 - double($length/2)]
	set pad_y1 [expr double($trace_width)/2 - double($width)/2]
	set pad_x2 $pad_x1
	set pad_y2 [expr -$pad_y1]
    }
    if {$pin == 1} {
	set pad_x1 $pad_x1
	set pad_x2 $pad_x2
    } else {
	set pad_x1 [expr -$pad_x1]
	set pad_x2 [expr -$pad_x2]
    }
    set textline "Pad \[ [format "%0.3fmm %0.3fmm" $pad_x1 $pad_y1] "
    append textline "[format "%0.3fmm %0.3fmm" $pad_x2 $pad_y2] "
    append textline "${trace_width}mm "
    # The copper pour clearance is 2x the actual clearance
    append textline "[format "%0.3fmm" [expr double($params(ps)) * 2]] "
    # The solder mask clearance is the entire opening
    append textline "[format "%0.3fmm" [expr double($params(ms)) * 2 + $trace_width]] "
    append textline "\"$pin\" \"$pin\" \"square\"\]"
    return $textline
}

proc square_pad_line {dimension} {
    # Return the pad line for a single square pad with no paste on it
    #
    # Arguments:
    #   dimension -- length and width of the pad (mm)
    global params
    set length $dimension
    set width $dimension
    set pin 1

    set trace_width $width
    set pad_x1 [expr double($trace_width)/2 - double($length)/2]
    set pad_y1 0
    set pad_x2 [expr -$pad_x1]
    set pad_y2 0

    set textline "Pad \[ [format "%0.3fmm %0.3fmm" $pad_x1 $pad_y1] "
    append textline "[format "%0.3fmm %0.3fmm" $pad_x2 $pad_y2] "
    append textline "${trace_width}mm "
    # The copper pour clearance is 2x the actual clearance
    append textline "[format "%0.3fmm" [expr double($params(ps)) * 2]] "
    # The solder mask clearance is the entire opening
    append textline "[format "%0.3fmm" [expr double($params(ms)) * 2 + $trace_width]] "
    append textline "\"$pin\" \"$pin\" \"square,nopaste\"\]"
    return $textline
}

proc masked_square_pad_line {dimension} {
    # Return the pad line for a single square pad covered with soldermask and no paste
    #
    # Arguments:
    #   dimension -- length and width of the pad (mm)
    global params
    set length $dimension
    set width $dimension
    puts "Creating masked square pad 1 of 1"
    set pin 1

    set trace_width $width
    set pad_x1 [expr double($trace_width)/2 - double($length)/2]
    set pad_y1 0
    set pad_x2 [expr -$pad_x1]
    set pad_y2 0

    set textline "Pad \[ [format "%0.3fmm %0.3fmm" $pad_x1 $pad_y1] "
    append textline "[format "%0.3fmm %0.3fmm" $pad_x2 $pad_y2] "
    append textline "${trace_width}mm "
    # The copper pour clearance is 2x the actual clearance
    append textline "[format "%0.3fmm" [expr double($params(ps)) * 2]] "
    # No solder mask opening
    append textline "[format "%0.3fmm" 0] "
    append textline "\"$pin\" \"$pin\" \"square,nopaste\"\]"
    return $textline
}

proc tst_pin_line {pin pins drill ring pitch} {
    # Return the pin line for one pin in a TST footprint
    #
    # Arguments:
    #   pin -- Pin number
    #   pins -- Total number of pins
    #   drill -- Drill diameter (mm)
    #   ring -- Width of annular ring (mm)
    #   pitch -- Pin pitch (mm)
    global params
    if [is_even [expr $pins/2]] {
	# Even number of pins for each row
	if {[is_even $pin]} {
	    # Pin is in top row
	    set index [expr $pin/2]
	    set pin_y [expr -double($pitch)/2]
	} else {
	    # Pin is in bottom row
	    set index [expr ($pin + 1)/2]
	    set pin_y [expr double($pitch)/2]
	}
	set pin_x [expr double($pitch) * ($index - $pins/4 - 0.5)]
    } else {
	# Odd number of pins for each row
	if {[is_even $pin]} {
	    # Pin is in top row
	    set index [expr $pin/2]
	    set pin_y [expr -double($pitch)/2]
	} else {
	    # Pin is in bottom row
	    set index [expr ($pin + 1)/2]
	    set pin_y [expr double($pitch)/2]
	}
	set pin_x [expr double($pitch) * ($index - 1 - ($pins - 2)/4)]
    }
    # The thickness parameter is the total copper diameter
    set thickness [expr $drill + double($ring) * 2]
    set textline "Pin \[ [format "%0.3fmm %0.3fmm" $pin_x $pin_y] "
    append textline "[format "%0.3fmm" $thickness] "
    # The copper pour clearance is 2x the actual clearance
    append textline "[format "%0.3fmm" [expr double($params(ps)) * 2]] "
    # The solder mask clearance is the entire opening
    append textline "[format "%0.3fmm" [expr double($params(ms)) * 2 + $thickness]] "
    append textline "[format "%0.3fmm" $drill] "
    append textline "\"$pin\" \"$pin\" "
    if [string match 1 $pin] {
	append textline "\"square\""
    } else {
	append textline "\"\""
    }
    append textline "\]"
    return $textline
}

proc shf_mthole_list {} {
    # Return the pin line list for the SHF mounting holes
    #
    # Arguments:
    #   none
    global params
    # Drill diameter (mm)
    set drill 1.19
    set pin_y 0
    set pin_x [expr ($params(n)/2 * 1.270 + 5.385)/2]
    set pin_x_list [list $pin_x -$pin_x]
    set pin_number [expr $params(n) + 1]
    foreach x_pos $pin_x_list {
	set thickness 0
	set textline "Pin \[ [format "%0.3fmm %0.3fmm" $x_pos $pin_y] "
	append textline "[format "%0.3fmm" $thickness] "
	# The copper pour clearance is 2x the actual clearance
	append textline "[format "%0.3fmm" [expr double($params(ps)) * 2]] "
	# The solder mask clearance is the entire opening
	append textline "[format "%0.3fmm" [expr double($params(ms)) * 2 + $thickness]] "
	append textline "[format "%0.3fmm" $drill] "
	append textline "\"$pin_number\" \"$pin_number\" "
	append textline "\"hole\""
	append textline "\]"
	lappend mthole_list $textline
	incr pin_number
    }
    return $mthole_list
}

proc snowman_slot_list {center_x_mm center_y_mm length_mm width_mm thickness_mm holes} {
    # Return the pin line list for a slot made up of round holes
    #
    # Arguments:
    #   center_x_mm -- x position of the center of the slot
    #   center_y_mm -- y position of the center of the slot
    #   length_mm -- Overall length of the slot
    #   width_mm -- Maximum width of the slot, same as the drill diameter
    #   thickness_mm -- Diameter of the copper circle drilled by each hole (mm)
    #   holes -- Number of holes to use (at least 2)
    global params
    set y_max_mm [expr ($length_mm / 2) - ($width_mm / 2)]
    set y_incr_mm [expr (2 * $y_max_mm) / ($holes - 1)]
    set pin_name "mt"
    foreach hole_number [iterint 0 $holes] {
	set pin_y [expr $center_y_mm + $y_max_mm - $y_incr_mm * $hole_number]
	set pin_x $center_x_mm
	set textline "Pin \[ [format "%0.3fmm %0.3fmm" $pin_x $pin_y] "
	append textline "[format "%0.3fmm" $thickness_mm] "
	# The copper pour clearance is 2x the actual clearance
	append textline "[format "%0.3fmm" [expr double($params(ps)) * 2]] "
	# The solder mask clearance is the entire opening
	append textline "[format "%0.3fmm" [expr double($params(ms)) * 2 + $thickness_mm]] "
	append textline "[format "%0.3fmm" $width_mm] "
	append textline "\"$pin_name\" \"$pin_name\" "
	append textline "\"\""
	append textline "\]"
	lappend mthole_list $textline
    }
    return $mthole_list
}

proc single_hole {pin drill ring plated} {
    # Return the pin line for a single unplated hole
    #
    # Arguments:
    #   pin -- Pin number
    #   drill -- Drill diameter (mm)
    #   ring -- Width of annular ring (mm)
    #   plated -- True or False
    global params
    set pin_y 0
    set pin_x 0
    set pin_number $pin
    set textline "Pin \[ [format "%0.3fmm %0.3fmm" $pin_x $pin_y] "

    if $plated {
	# The thickness parameter is the total copper diameter
	set thickness [expr $drill + double($ring) * 2]

    } else {
	set thickness 0

    }
    append textline "[format "%0.3fmm" $thickness] "
    # The copper pour clearance is 2x the actual clearance
    append textline "[format "%0.3fmm" [expr double($params(ps)) * 2]] "

    # The solder mask clearance is the entire opening
    append textline "[format "%0.3fmm" [expr double($params(ms)) * 2 + $thickness]] "
    append textline "[format "%0.3fmm" $drill] "
    append textline "\"$pin_number\" \"$pin_number\" "
    if $plated {
	append textline "\"\""
    } else {
	append textline "\"hole\""
    }
    append textline "\]"
    return $textline
}

proc list_pin_line {x_mm y_mm name drill_mm thickness_mm} {
    # Return the pin line for one pin in a pin location list
    #
    # Arguments:
    #   x -- x location (mm)
    #   y -- y location (mm)
    #   name -- The pin named used for referencing the pin
    #   drill -- Drill diameter (mm)
    #   thickness -- Total copper pad diameter (mm)
    #
    # Pin [x y thickness clearance mask drill name number flags]
    global params
    puts "Creating pin $name of a pin list"
    set pin_x $x_mm
    set pin_y $y_mm
    set textline "Pin \[ [format "%0.3fmm %0.3fmm" $pin_x $pin_y] "
    append textline "[format "%0.3fmm" $thickness_mm] "
    # The copper pour clearance is 2x the actual clearance
    append textline "[format "%0.3fmm" [expr double($params(ps)) * 2]] "
    # The solder mask clearance is the entire opening
    append textline "[format "%0.3fmm" [expr double($params(ms)) * 2 + $thickness_mm]] "
    append textline "[format "%0.3fmm" $drill_mm] "
    append textline "\"$name\" \"$name\" "
    if {$name eq 1} {
	append textline "\"square\""
    } else {
	append textline "\"\""
    }
    append textline "\]"
    return $textline
}

proc tsws_pin_line {pin pins drill ring pitch} {
    # Return the pin line for one pin in a single-row TSW footprint
    #
    # Arguments:
    #   pin -- Pin number
    #   pins -- Total number of pins
    #   drill -- Drill diameter (mm)
    #   ring -- Width of annular ring (mm)
    #   pitch -- Pin pitch (mm)
    global params
    # Index starts at 1
    puts "Creating TSWS pin $pin of $pins"
    set index $pin
    set pin_y 0
    if [is_even [expr $pins]] {
	# Even number of pins
	set pin_x [expr double($pitch) * ($index - double($pins)/2 - 0.5)]
    } else {
	# Odd number of pins
	set pin_x [expr double($pitch) * ($index - 1 - double($pins - 1)/2)]
    }
    # The thickness parameter is the total copper diameter
    set thickness [expr $drill + double($ring) * 2]
    set textline "Pin \[ [format "%0.3fmm %0.3fmm" $pin_x $pin_y] "
    append textline "[format "%0.3fmm" $thickness] "
    # The copper pour clearance is 2x the actual clearance
    append textline "[format "%0.3fmm" [expr double($params(ps)) * 2]] "
    # The solder mask clearance is the entire opening
    append textline "[format "%0.3fmm" [expr double($params(ms)) * 2 + $thickness]] "
    append textline "[format "%0.3fmm" $drill] "
    append textline "\"$pin\" \"$pin\" "
    if [string match 1 $pin] {
	append textline "\"square\""
    } else {
	append textline "\"\""
    }
    append textline "\]"
    return $textline
}

proc lmzfl_pin_line {pin pins drill ring pitch} {
    # Return the pin line for one pin in a LMZFL footprint
    #
    # LMZFL terminal blocks connect 1-2, 3-4, and so on.  So a 4-pin
    # part actually only has two connections.  We'll rename the pins
    # to reflect the connectivity.  So pin 2 becomes pin 1, pin 4
    # becomes pin 2, and so on.
    #
    # Arguments:
    #   pin -- Pin number
    #   pins -- Total number of pins
    #   drill -- Drill diameter (mm)
    #   ring -- Width of annular ring (mm)
    #   pitch -- Pin pitch (mm)
    global params
    if [is_even [expr $pins/2]] {
	# Even number of pins for each row
	if {[is_even $pin]} {
	    # Pin is in top row
	    set index [expr $pin/2]
	    set pin_y [expr -double($pitch)/2]
	} else {
	    # Pin is in bottom row
	    set index [expr ($pin + 1)/2]
	    set pin_y [expr double($pitch)/2]
	}
	set pin_x [expr double($pitch) * ($index - $pins/4 - 0.5)]
    } else {
	# Odd number of pins for each row
	if {[is_even $pin]} {
	    # Pin is in top row
	    set index [expr $pin/2]
	    set pin_y [expr -double($pitch)/2]
	} else {
	    # Pin is in bottom row
	    set index [expr ($pin + 1)/2]
	    set pin_y [expr double($pitch)/2]
	}
	set pin_x [expr double($pitch) * ($index - 1 - ($pins - 2)/4)]
    }
    # The thickness parameter is the total copper diameter
    set thickness [expr $drill + double($ring) * 2]
    set textline "Pin \[ [format "%0.3fmm %0.3fmm" $pin_x $pin_y] "
    append textline "[format "%0.3fmm" $thickness] "
    # The copper pour clearance is 2x the actual clearance
    append textline "[format "%0.3fmm" [expr double($params(ps)) * 2]] "
    # The solder mask clearance is the entire opening
    append textline "[format "%0.3fmm" [expr double($params(ms)) * 2 + $thickness]] "
    append textline "[format "%0.3fmm" $drill] "
    append textline "\"$index\" \"$index\" "
    if [string match 1 $index] {
	append textline "\"square\""
    } else {
	append textline "\"\""
    }
    append textline "\]"
    return $textline
}

proc mta_pin_line {pin pins drill ring pitch} {
    # Return the pin line for one pin in a mta-series footprint
    #
    # Arguments:
    #   pin -- Pin number
    #   pins -- Total number of pins
    #   drill -- Drill diameter (mm)
    #   ring -- Width of annular ring (mm)
    #   pitch -- Pin pitch (mm)
    global params
    puts "Creating pin $pin of a MTA-100 header"
    set index $pin
    set pin_y 0
    if [is_even [expr $pins]] {
	# Even number of pins
	set pin_x [expr double($pitch) * ($index - $pins/2 - 0.5)]
    } else {
	# Odd number of pins
	set pin_x [expr double($pitch) * ($index - 1 - ($pins - 1)/2)]
    }
    # The thickness parameter is the total copper diameter
    set thickness [expr $drill + double($ring) * 2]
    set textline "Pin \[ [format "%0.3fmm %0.3fmm" $pin_x $pin_y] "
    append textline "[format "%0.3fmm" $thickness] "
    # The copper pour clearance is 2x the actual clearance
    append textline "[format "%0.3fmm" [expr double($params(ps)) * 2]] "
    # The solder mask clearance is the entire opening
    append textline "[format "%0.3fmm" [expr double($params(ms)) * 2 + $thickness]] "
    append textline "[format "%0.3fmm" $drill] "
    append textline "\"$pin\" \"$pin\" "
    if [string match 1 $pin] {
	append textline "\"square\""
    } else {
	append textline "\"\""
    }
    append textline "\]"
    return $textline
}

proc slug_lines {pin padx pady maskx masky} {
    # Return two pad lines (in a list) for the PowerPAD under a part
    #
    # Arguments:
    #   pin -- Pin number
    #   padx -- X dimension of pad (floating point mm)
    #   pady -- Y dimension of pad (floating point mm)
    #   maskx -- X dimension of solder mask opening (floating point mm)
    #   masky -- Y dimension of solder mask opening (floating point mm)
    set pad_x1 [expr -double($padx)/2 + double($pady)/2]
    set pad_x2 [expr double($padx)/2 - double($pady)/2]
    set textline "Pad \[ [format "%0.3fmm %0.3fmm" $pad_x1 0] "
    append textline "[format "%0.3fmm %0.3fmm" $pad_x2 0] "
    append textline "${pady}mm "
    # This is a solder mask defined pad -- no separation to copper planes / pours
    append textline "[format "%0.3fmm" 0] "
    # This pad is completely covered in solder mask
    append textline "[format "%0.3fmm" 0] "
    append textline "\"$pin\" \"$pin\" \"square\"\]"
    set padline $textline
    ###################### Solder mask opening #######################
    set pad_x1 [expr -double($maskx)/2 + double($masky)/2]
    set pad_x2 [expr double($maskx)/2 - double($masky)/2]
    set textline "Pad \[ [format "%0.3fmm %0.3fmm" $pad_x1 0] "
    append textline "[format "%0.3fmm %0.3fmm" $pad_x2 0] "
    append textline "${pady}mm "
    # No separation to copper planes / pours
    append textline "[format "%0.3fmm" 0] "
    # Solder mask opening has same dimensions as pad
    append textline "[format "%0.3fmm" $masky] "
    append textline "\"$pin\" \"$pin\" \"square\"\]"
    set maskline $textline
    return [list $padline $maskline]
}

proc slug_vias {pin nx ny drill pitch} {
    # Return list of pin lines for vias inside PowerPAD
    #
    # Arguments:
    #   pin -- Pin number (all will have the same number)
    #   nx -- Number of vias in the x direction
    #   ny -- Number of vias in the y direction
    #   drill -- Drill diameter (mm)
    #   pitch -- Drill pitch (mm)
    #
    # (column,row)
    #
    # (1,1) (2,1) (3,1) ...
    # (1,2) (2,2) (3,2) ...
    # (1,3) (2,3) (3,3) ...
    # ...
    global params
    set pin_list [list]
    foreach row_index [iterint 1 $ny] {
	if {[is_even $ny]} {
	    set pin_y [expr double($pitch) * ($row_index - $ny/2 - 0.5)]
	} else {
	    set pin_y [expr double($pitch) * ($row_index - 1 - ($ny - 1)/2)]
	}
	foreach col_index [iterint 1 $nx] {
	    if {[is_even $nx]} {
		set pin_x [expr double($pitch) * ($col_index - $nx/2 - 0.5)]
	    } else {
		set pin_x [expr double($pitch) * ($col_index - 1 - ($nx - 1)/2)]
	    }
	    # The thickness parameter is the total copper diameter.
	    # This can't be the drill diameter unless the hole is
	    # non-plated.  Set the annulus to be 0.152mm (6 mils)
	    set thickness [expr $drill + 2 * 0.1524]
	    set textline "Pin \[ [format "%0.3fmm %0.3fmm" $pin_x $pin_y] "
	    append textline "[format "%0.3fmm" $thickness] "
	    # The copper pour clearance is 2x the actual clearance
	    append textline "[format "%0.3fmm" [expr 2 * $params(ps)]] "
	    # The solder mask clearance is the entire opening
	    append textline "[format "%0.3fmm" $drill] "
	    append textline "[format "%0.3fmm" $drill] "
	    append textline "\"$pin\" \"$pin\" "
	    # Special flags
	    append textline "\"\""
	    append textline "\]"
	    lappend pin_list $textline
	}
    }
    return $pin_list
}

proc dim_to_num {dimension} {
    # Return the numeric value of the dimension
    #
    # Arguments:
    #   dimension -- Number like 3.02mm
    set splitstr [split $dimension ""]
    foreach character $splitstr {
	if [string match {[0-9 + \- .]} $character] {
	    append numstr $character
	}
    }
    return $numstr
}

proc dim_to_unit {dimension} {
    # Return the measurement unit of the dimension
    #
    # Arguments:
    #   dimension -- Number like 3.02mm
    set splitstr [split $dimension ""]
    foreach character $splitstr {
	if [string match {[milmm]} $character] {
	    append unitstr $character
	}
    }
    return $unitstr
}

proc pad_outer_extremes {pad_line_list} {
    # Return the lower left and upper right coordinates of the
    # "extreme outer" pad points -- the points farthest from the center of
    # the footprint.  For example:
    #
    # -1.7 5.1 1.7 -5.1
    #
    # would be the return list for pads with a lower left corner of
    # (-1.7, 5.1) and an upper right corner of (1.7, -5.1)
    set xmin 0
    set ymin 0
    set xmax 0
    set ymax 0
    # Assume everything is in the same units
    foreach padline $pad_line_list {
	set padxraw [lindex [split $padline] 2]
	set padx [dim_to_num $padxraw]
	set pady1raw [lindex [split $padline] 3]
	set pady1 [dim_to_num $pady1raw]
	set pady2raw [lindex [split $padline] 5]
	set pady2 [dim_to_num $pady2raw]
	if {$padx < $xmin} {set xmin $padx}
	if {$padx > $xmax} {set xmax $padx}
	if {$pady1 < $ymin} {set ymin $pady1}
	if {$pady1 > $ymax} {set ymax $pady1}
	if {$pady2 < $ymin} {set ymin $pady2}
	if {$pady2 > $ymax} {set ymax $pady2}
    }
    return [list $xmin $ymax $xmax $ymin]
}

proc pin_outer_extremes {pin_line_list} {
    # Return the lower left and upper right coordinates of the
    # "extreme outer" pin points -- the points farthest from the center of
    # the footprint.  For example:
    #
    # -1.7 5.1 1.7 -5.1
    #
    # would be the return list for pins with a lower left corner of
    # (-1.7, 5.1) and an upper right corner of (1.7, -5.1)
    set xmin 0
    set ymin 0
    set xmax 0
    set ymax 0
    # Assume everything is in the same units
    foreach pinline $pin_line_list {
	set pinxraw [lindex [split $pinline] 2]
	set pinx [dim_to_num $pinxraw]
	set pinyraw [lindex [split $pinline] 3]
	set piny [dim_to_num $pinyraw]
	if {$pinx < $xmin} {set xmin $pinx}
	if {$pinx > $xmax} {set xmax $pinx}
	if {$piny < $ymin} {set ymin $piny}
	if {$piny > $ymax} {set ymax $piny}
    }
    return [list $xmin $ymax $xmax $ymin]
}

proc pad_inner_extremes {pad_line_list} {
    # Return the lower left and upper right coordinates of the
    # "extreme inner" pad points -- the points closest to the center
    # of the footprint.
    set xmin 0
    set ymin 1000
    set xmax 0
    # Assume everything is in the same units
    foreach padline $pad_line_list {
	set padxraw [lindex [split $padline] 2]
	set padx [dim_to_num $padxraw]
	set pady1raw [lindex [split $padline] 3]
	set pady1 [dim_to_num $pady1raw]
	set pady2raw [lindex [split $padline] 5]
	set pady2 [dim_to_num $pady2raw]
	if {$padx < $xmin} {set xmin $padx}
	if {$padx > $xmax} {set xmax $padx}
	if {[expr abs($pady1)] < $ymin} {set ymin $pady1}
	if {[expr abs($pady2)] < $ymin} {set ymin $pady2}
    }
    return [list $xmin $ymin $xmax -$ymin]
}

proc dil_courtyard_lines {pad_line_list} {
    # Return a list of silkscreen lines for the courtyard boundary
    #
    # Arguments:
    #   pad_line_list -- List of pad lines created by dil_pad_line
    global params
    puts "Making the DIL pad footprint courtyard boundary and pin 1 dot"
    # Offset from the lower left corner to the dot
    set dot_offset_mm 0.5
    set dot_diameter_mm 0.5
    lassign [pad_outer_extremes $pad_line_list] xmin ymax xmax ymin
    # Actual copper boundaries will have a 1/2 thickness value added
    # to them.  For DIL pads, all pads are the same width.
    set padline [lindex $pad_line_list 0]
    set pad_width_mm [dim_to_num [lindex [split $padline] 6]]
    set bbox_xmin [expr $xmin - double($pad_width_mm)/2]
    set bbox_xmax [expr $xmax + double($pad_width_mm)/2]
    set bbox_ymin [expr $ymin - double($pad_width_mm)/2]
    set bbox_ymax [expr $ymax + double($pad_width_mm)/2]
    # We'll go clockwise around the courtyard starting in the lower left corner
    set x1 [expr $bbox_xmin - $params(cs)]
    set y1 [expr $bbox_ymax + $params(cs)]
    set x2 $x1
    set y2 [expr $bbox_ymin - $params(cs)]
    set x3 [expr $bbox_xmax + $params(cs)]
    set y3 $y2
    set x4 $x3
    set y4 $y1
    # Define the pin 1 dot
    set xdot [expr $x1 - $dot_offset_mm]
    set ydot $ymax
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    set lines [list]
    lappend lines [format $formatstr $x1 $y1 $x2 $y2 $params(lt)]
    lappend lines [format $formatstr $x2 $y2 $x3 $y3 $params(lt)]
    lappend lines [format $formatstr $x3 $y3 $x4 $y4 $params(lt)]
    lappend lines [format $formatstr $x4 $y4 $x1 $y1 $params(lt)]
    set formatstr "ElementArc\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3f %0.3f %0.3fmm \]"
    lappend lines [format $formatstr $xdot $ydot \
		       [expr double($dot_diameter_mm)/4] \
		       [expr double($dot_diameter_mm)/4] \
		       0 359 \
		       [expr double($dot_diameter_mm)/2]]
    return $lines
}

proc dbz_courtyard_lines {pad_width body_length body_width} {
    # Return a list of silkscreen lines for the courtyard boundary
    #
    # Arguments:
    #   pad_width -- Width of one pad (mm)
    #   body_length -- Long body dimension (mm)
    #   body_width -- Short body dimension (mm)
    global params
    set buffer_mm 0.2
    set x(1) [expr -double($body_length) / 2]
    set y(1) [expr double($body_width) / 2]
    set x(2) $x(1)
    set y(2) [expr -$y(1)]
    set x(3) [expr -double($pad_width) / 2 - $buffer_mm]
    set y(3) $y(2)
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    foreach point [iterint 1 2] {
	set nextpoint [expr $point + 1]
	lappend lines [format $formatstr $x($point) $y($point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    set x(1) [expr double($body_length) / 2]
    set y(1) [expr double($body_width) / 2]
    set x(2) $x(1)
    set y(2) [expr -$y(1)]
    set x(3) [expr double($pad_width) / 2 + $buffer_mm]
    set y(3) $y(2)
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    foreach point [iterint 1 2] {
	set nextpoint [expr $point + 1]
	lappend lines [format $formatstr $x($point) $y($point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    return $lines
}

proc smbth_courtyard_lines {} {
    global params
    # Main connector body
    set body_length_mm 6.4
    set x(1) [expr -double($body_length_mm) / 2]
    set y(1) [expr double($body_length_mm) / 2]
    set x(2) $x(1)
    set y(2) [expr -$y(1)]
    set x(3) [expr -$x(1)]
    set y(3) [expr $y(2)]
    set x(4) $x(3)
    set y(4) $y(1)
    set x(5) $x(1)
    set y(5) $y(1)
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    foreach point [iterint 1 4] {
	set nextpoint [expr $point + 1]
	lappend lines [format $formatstr $x($point) $y($point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    # SMB part
    set snout_width_mm 6.4
    set snout_length_mm 8.2
    set x(1) [expr -double($snout_width_mm) / 2]
    set y(1) [expr -double($body_length_mm) / 2]
    set x(2) $x(1)
    set y(2) [expr $y(1) - $snout_length_mm]
    set x(3) [expr -$x(1)]
    set y(3) [expr $y(2)]
    set x(4) $x(3)
    set y(4) $y(1)
    set x(5) $x(1)
    set y(5) $y(1)
    foreach point [iterint 1 4] {
	set nextpoint [expr $point + 1]
	lappend lines [format $formatstr $x($point) $y($point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    return $lines
}

proc sj5_courtyard_lines {} {
    global params
    # Make the body diameter a bit larger than the actual diameter to avoid covering copper with silkscreen
    set body_diameter_mm 8.5
    set formatstr "ElementArc\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3f %0.3f %0.3fmm \]"
    lappend lines [format $formatstr 0 0 \
		       [expr double($body_diameter_mm)/2] \
		       [expr double($body_diameter_mm)/2] \
		       0 359 \
		       $params(lt)]
    return $lines
}

proc round_mthole_boundary_line {diameter} {
    # Return a silkscreen line for the mounting hole body
    #
    # Arguments:
    #   diameter -- Standoff body diameter (mm)
    global params

    set formatstr "ElementArc\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3f %0.3f %0.3fmm \]"
    lappend line_list [format $formatstr 0 0 \
			   [expr double($diameter)/2] [expr double($diameter)/2] \
			   0 359 $params(lt)]
    return $line_list
}

proc circle_courtyard_line {diameter} {
    # Return a circle silkscreen line
    #
    # Arguments:
    #   diameter -- Standoff body diameter (mm)
    global params

    set formatstr "ElementArc\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3f %0.3f %0.3fmm \]"
    lappend line_list [format $formatstr 0 0 \
			   [expr double($diameter)/2] [expr double($diameter)/2] \
			   0 359 $params(lt)]
    return $line_list
}

proc chip_courtyard_lines {pad_line_list} {
    # Return a list of silkscreen lines for the courtyard boundary
    #
    # Arguments:
    #   pad_line_list -- List of pad lines created by chip_pad_line
    global params
    lassign [pad_outer_extremes $pad_line_list] xmin ymax xmax ymin
    # Actual copper boundaries will have a 1/2 thickness value added
    # to them.  For DIL pads, all pads are the same width.
    set padline [lindex $pad_line_list 0]
    set pad_width_mm [dim_to_num [lindex [split $padline] 6]]
    set bbox_xmin [expr $xmin - double($pad_width_mm)/2]
    set bbox_xmax [expr $xmax + double($pad_width_mm)/2]
    set bbox_ymin [expr $ymin - double($pad_width_mm)/2]
    set bbox_ymax [expr $ymax + double($pad_width_mm)/2]
    # We'll go clockwise around the courtyard starting in the lower left corner
    set x1 [expr $bbox_xmin - $params(cs)]
    set y1 [expr $bbox_ymax + $params(cs)]
    set x2 $x1
    set y2 [expr $bbox_ymin - $params(cs)]
    set x3 [expr $bbox_xmax + $params(cs)]
    set y3 $y2
    set x4 $x3
    set y4 $y1
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    set lines [list]
    lappend lines [format $formatstr $x1 $y1 $x2 $y2 $params(lt)]
    lappend lines [format $formatstr $x2 $y2 $x3 $y3 $params(lt)]
    lappend lines [format $formatstr $x3 $y3 $x4 $y4 $params(lt)]
    lappend lines [format $formatstr $x4 $y4 $x1 $y1 $params(lt)]
    return $lines
}

proc tst_boundary_lines {} {
    # Return a list of silkscreen lines describing the outside of the
    # tst connector
    global params
    set width [expr ($params(n)/2) * 2.54 + 7.62]
    set height 9.27
    set polarity_gap [expr 2 * 2.54]
    # We'll go clockwise around the box starting in the lower left corner
    set x1 [expr -double($width)/2]
    set y1 [expr double($height)/2]
    set x2 [expr -double($width)/2]
    set y2 [expr -double($height)/2]
    set x3 [expr double($width)/2]
    set y3 [expr -double($height)/2]
    set x4 [expr double($width)/2]
    set y4 [expr double($height)/2]
    set x5 [expr double($polarity_gap)/2]
    set y5 [expr double($height)/2]
    set x6 [expr -double($polarity_gap)/2]
    set y6 [expr double($height)/2]
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    lappend lines [format $formatstr $x1 $y1 $x2 $y2 $params(lt)]
    lappend lines [format $formatstr $x2 $y2 $x3 $y3 $params(lt)]
    lappend lines [format $formatstr $x3 $y3 $x4 $y4 $params(lt)]
    lappend lines [format $formatstr $x4 $y4 $x5 $y5 $params(lt)]
    lappend lines [format $formatstr $x6 $y6 $x1 $y1 $params(lt)]
    return $lines
}

proc tstsm_boundary_lines {} {
    # Return a list of boundary lines describing the outside of the
    # HTST surface-mount connector
    global params
    set width [expr ($params(n)/2) * 2.54 + 7.62]
    set height 9.27
    set x(1) [expr -$width/2 + 4]
    set y(1) [expr $height/2]
    set x(2) [expr -$width/2]
    set y(2) $y(1)
    set x(3) $x(2)
    set y(3) [expr -$height/2]
    set x(4) $x(1)
    set y(4) $y(3)
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    foreach point [iterint 1 3] {
	set nextpoint [expr $point + 1]
	lappend lines [format $formatstr $x($point) $y($point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    set x(1) [expr $width/2 - 4]
    set y(1) [expr $height/2]
    set x(2) [expr $width/2]
    set y(2) $y(1)
    set x(3) $x(2)
    set y(3) [expr -$height/2]
    set x(4) $x(1)
    set y(4) $y(3)
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    foreach point [iterint 1 3] {
	set nextpoint [expr $point + 1]
	lappend lines [format $formatstr $x($point) $y($point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    return $lines
}

proc shf_boundary_lines {} {
    # Return a list of silkscreen lines describing the outside of the
    # shf connector
    global params
    set width [expr ($params(n)/2) * 1.27 + 6.35]
    set height [expr 5.08 + 0.51]
    set polarity_gap [expr 2 * 1.27]
    # We'll go clockwise around the box starting in the lower left corner
    set x1 [expr -double($width)/2]
    set y1 [expr double($height)/2]
    set x2 [expr -double($width)/2]
    set y2 [expr -double($height)/2]
    set x3 [expr double($width)/2]
    set y3 [expr -double($height)/2]
    set x4 [expr double($width)/2]
    set y4 [expr double($height)/2]
    set x5 [expr double($polarity_gap)/2]
    set y5 [expr double($height)/2]
    set x6 [expr -double($polarity_gap)/2]
    set y6 [expr double($height)/2]
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    lappend lines [format $formatstr $x1 $y1 $x2 $y2 $params(lt)]
    lappend lines [format $formatstr $x2 $y2 $x3 $y3 $params(lt)]
    lappend lines [format $formatstr $x3 $y3 $x4 $y4 $params(lt)]
    lappend lines [format $formatstr $x4 $y4 $x5 $y5 $params(lt)]
    lappend lines [format $formatstr $x6 $y6 $x1 $y1 $params(lt)]
    return $lines
}

proc tsws_boundary_lines {} {
    # Return a list of silkscreen lines describing the outside of the
    # single-row tsw connector
    global params
    set width [expr $params(n) * 2.54]
    set height 2.54
    # Offset from the lower left corner to the dot
    set dot_offset_mm 0.5
    set dot_diameter_mm 0.5
    # We'll go clockwise around the box starting in the lower left corner
    set x1 [expr -double($width)/2]
    set y1 [expr double($height)/2]
    set x2 [expr -double($width)/2]
    set y2 [expr -double($height)/2]
    set x3 [expr double($width)/2]
    set y3 [expr -double($height)/2]
    set x4 [expr double($width)/2]
    set y4 [expr double($height)/2]
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    lappend lines [format $formatstr $x1 $y1 $x2 $y2 $params(lt)]
    lappend lines [format $formatstr $x2 $y2 $x3 $y3 $params(lt)]
    lappend lines [format $formatstr $x3 $y3 $x4 $y4 $params(lt)]
    lappend lines [format $formatstr $x4 $y4 $x1 $y1 $params(lt)]
    # Define the pin 1 dot
    set xdot [expr $x1 - $dot_offset_mm]
    set ydot [expr $y1 + $dot_offset_mm]
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    set lines [list]
    lappend lines [format $formatstr $x1 $y1 $x2 $y2 $params(lt)]
    lappend lines [format $formatstr $x2 $y2 $x3 $y3 $params(lt)]
    lappend lines [format $formatstr $x3 $y3 $x4 $y4 $params(lt)]
    lappend lines [format $formatstr $x4 $y4 $x1 $y1 $params(lt)]
    set formatstr "ElementArc\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3f %0.3f %0.3fmm \]"
    lappend lines [format $formatstr $xdot $ydot \
		       [expr double($dot_diameter_mm)/4] \
		       [expr double($dot_diameter_mm)/4] \
		       0 359 \
		       [expr double($dot_diameter_mm)/2]]
    return $lines
}

proc tswsra_boundary_lines {} {
    # Return a list of silkscreen lines describing the outside of the
    # single-row right-angle tsw connector
    global params
    set lines [list]

    # Go counter-clockwise around the pins starting in the lower left corner
    set width [expr $params(n) * 2.54 - 2.54]
    set height [expr 0.22 * 25.4]
    set x1 [expr -double($width)/2]
    set y1 [expr -3 * 5.08 / 4]
    set x2 [expr -$x1]
    set y2 $y1
    set x3 $x2
    set y3 [expr $y1 - $height]
    set x4 $x1
    set y4 $y3
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    lappend lines [format $formatstr $x1 $y1 $x2 $y2 $params(lt)]
    lappend lines [format $formatstr $x2 $y2 $x3 $y3 $params(lt)]
    lappend lines [format $formatstr $x3 $y3 $x4 $y4 $params(lt)]
    lappend lines [format $formatstr $x4 $y4 $x1 $y1 $params(lt)]

    # We'll go counter-clockwise around the box starting in the lower left corner
    set width [expr $params(n) * 2.54]
    set height 5.08
    # Offset from the lower left corner to the dot
    set dot_offset_mm 0.5
    set dot_diameter_mm 0.5
    set x1 [expr -double($width)/2]
    set y1 [expr double($height)/4]
    set x2 [expr -$x1]
    set y2 $y1
    set x3 $x2
    set y3 [expr -3 * double($height)/4]
    set x4 $x1
    set y4 $y3
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    lappend lines [format $formatstr $x1 $y1 $x2 $y2 $params(lt)]
    lappend lines [format $formatstr $x2 $y2 $x3 $y3 $params(lt)]
    lappend lines [format $formatstr $x3 $y3 $x4 $y4 $params(lt)]
    lappend lines [format $formatstr $x4 $y4 $x1 $y1 $params(lt)]
    # Define the pin 1 dot
    set xdot [expr $x1 - $dot_offset_mm]
    set ydot [expr $y1 + $dot_offset_mm]
    set formatstr "ElementArc\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3f %0.3f %0.3fmm \]"
    lappend lines [format $formatstr $xdot $ydot \
		       [expr double($dot_diameter_mm)/4] \
		       [expr double($dot_diameter_mm)/4] \
		       0 359 \
		       [expr double($dot_diameter_mm)/2]]
    return $lines
}

proc jstph_boundary_lines {} {
    # Return a list of silkscreen lines describing the outside of the
    # single-row right-angle JST-PH connector
    global params
    puts "Creating JSTPH boundary lines"
    set lines [list]

    # Go counter-clockwise around the pins starting in the lower left corner
    set width [expr ($params(n)-1) * 2.0 + 3.9]
    set x1 [expr -double($width)/2]
    set y1 2.8
    set x2 $x1
    set y2 [expr -1.7]
    set x3 [expr -$x2]
    set y3 $y2
    set x4 $x3
    set y4 $y1
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    lappend lines [format $formatstr $x1 $y1 $x2 $y2 $params(lt)]
    lappend lines [format $formatstr $x2 $y2 $x3 $y3 $params(lt)]
    lappend lines [format $formatstr $x3 $y3 $x4 $y4 $params(lt)]
    lappend lines [format $formatstr $x4 $y4 $x1 $y1 $params(lt)]

    # Offset from the lower left corner to the dot
    set dot_offset_mm 0.5
    set dot_diameter_mm 0.5

    # Define the pin 1 dot
    set xdot [expr $x1 - $dot_offset_mm]
    set ydot 0
    set formatstr "ElementArc\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3f %0.3f %0.3fmm \]"
    lappend lines [format $formatstr $xdot $ydot \
		       [expr double($dot_diameter_mm)/4] \
		       [expr double($dot_diameter_mm)/4] \
		       0 359 \
		       [expr double($dot_diameter_mm)/2]]
    return $lines
}

proc mta100_boundary_lines {} {
    # Return a list of silkscreen lines describing the outside of the
    # MTA-100 connector
    global params
    puts "Creating MTA-100 boundary lines"
    set width_mm [expr ($params(n) - 1) * 2.54 + 2.54]
    set tab_thickness_mm 1.4
    # We'll go clockwise around the box starting in the lower left corner
    set x1 [expr -double($width_mm)/2]
    set y1 3.17
    set x2 $x1
    set y2 [expr $y1 - $tab_thickness_mm]
    set x3 $x1
    set y3 -2.55
    set x4 [expr -$x1]
    set y4 $y3
    set x5 $x4
    set y5 $y2
    set x6 $x4
    set y6 $y1
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    lappend lines [format $formatstr $x1 $y1 $x3 $y3 $params(lt)]
    lappend lines [format $formatstr $x3 $y3 $x4 $y4 $params(lt)]
    lappend lines [format $formatstr $x4 $y4 $x6 $y6 $params(lt)]
    lappend lines [format $formatstr $x6 $y6 $x1 $y1 $params(lt)]
    lappend lines [format $formatstr $x2 $y2 $x5 $y5 $params(lt)]
    return $lines
}

proc musbr_boundary_lines {} {
    # Return a lists of silkscreen lines describing the outside of the
    # MUSBR connector.
    global params
    # We'll go clockwise around the box starting in the lower left
    # corner of the main body.
    set x1 -11.25
    set y1 7.175
    set x2 $x1
    set y2 [expr $y1 - 10.25]
    set x3 [expr -$x1]
    set y3 $y2
    set x4 [expr -$x1]
    set y4 $y1
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    lappend lines [format $formatstr $x1 $y1 $x2 $y2 $params(lt)]
    lappend lines [format $formatstr $x2 $y2 $x3 $y3 $params(lt)]
    lappend lines [format $formatstr $x3 $y3 $x4 $y4 $params(lt)]
    lappend lines [format $formatstr $x4 $y4 $x1 $y1 $params(lt)]
    # Now do the flange that will ultimately be off the PCB
    set flange_x(1) -11.75
    set flange_y(1) [expr 10.25 - 3.075 + 2.2]
    set flange_x(2) $flange_x(1)
    set flange_y(2) [expr $flange_y(1) - 2.2]
    set flange_x(3) [expr -$flange_x(1)]
    set flange_y(3) $flange_y(2)
    set flange_x(4) $flange_x(3)
    set flange_y(4) $flange_y(1)
    foreach vertex [iterint 1 3] {
	lappend lines [format $formatstr $flange_x($vertex) $flange_y($vertex) \
			   $flange_x([expr $vertex + 1]) $flange_y([expr $vertex + 1]) \
			  $params(lt)]
    }
    lappend lines [format $formatstr $flange_x(4) $flange_y(4) \
		       $flange_x(1) $flange_y(1) $params(lt)]
    # Add the cap that will extend through the panel
    set cap_x(1) -5
    set cap_y(1) [expr $flange_y(1) + 2.5]
    set cap_x(2) $cap_x(1)
    set cap_y(2) [expr $cap_y(1) - 2.5]
    set cap_x(3) [expr -$cap_x(1)]
    set cap_y(3) $cap_y(2)
    set cap_x(4) $cap_x(3)
    set cap_y(4) $cap_y(1)
    foreach vertex [iterint 1 3] {
	lappend lines [format $formatstr $cap_x($vertex) $cap_y($vertex) \
			   $cap_x([expr $vertex + 1]) $cap_y([expr $vertex + 1]) \
			  $params(lt)]
    }
    lappend lines [format $formatstr $cap_x(4) $cap_y(4) \
		       $cap_x(1) $cap_y(1) $params(lt)]
    return $lines
}

proc anano_boundary_lines {} {
    # Return a lists of silkscreen lines describing the outside of the
    # Arduino Nano
    global params
    # We'll go clockwise around the box starting in the lower left
    # corner of the main body.
    set x1 -21.590
    set y1 8.89
    set x2 $x1
    set y2 [expr -$y1]
    set x3 [expr -$x1]
    set y3 $y2
    set x4 [expr -$x1]
    set y4 $y1
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    lappend lines [format $formatstr $x1 $y1 $x2 $y2 $params(lt)]
    lappend lines [format $formatstr $x2 $y2 $x3 $y3 $params(lt)]
    lappend lines [format $formatstr $x3 $y3 $x4 $y4 $params(lt)]
    lappend lines [format $formatstr $x4 $y4 $x1 $y1 $params(lt)]
    # Now do the USB connector that will ultimately be off the PCB
    set usb_x(1) [expr -$x1]
    set usb_y(1) 3.937
    set usb_x(2) $usb_x(1)
    set usb_y(2) [expr -$usb_y(1)]
    set usb_x(3) [expr $usb_x(1) + 2.5]
    set usb_y(3) $usb_y(2)
    set usb_x(4) $usb_x(3)
    set usb_y(4) $usb_y(1)
    foreach vertex [iterint 1 3] {
	lappend lines [format $formatstr $usb_x($vertex) $usb_y($vertex) \
			   $usb_x([expr $vertex + 1]) $usb_y([expr $vertex + 1]) \
			  $params(lt)]
    }
    lappend lines [format $formatstr $usb_x(4) $usb_y(4) \
		       $usb_x(1) $usb_y(1) $params(lt)]
    return $lines
}

proc astar_328pb_boundary_lines {} {
    # Return a lists of silkscreen lines describing the outside of the
    # Pololu A-star 328pb
    global params
    # We'll go clockwise around the box starting in the lower left
    # corner of the main body.
    set courtyard_x(1) [expr -0.65 * 25.4]
    set courtyard_y(1) [expr 0.35 * 25.4]
    set courtyard_x(2) $courtyard_x(1)
    set courtyard_y(2) [expr -$courtyard_y(1)]
    set courtyard_x(3) [expr -$courtyard_x(1)]
    set courtyard_y(3) $courtyard_y(2)
    set courtyard_x(4) $courtyard_x(3)
    set courtyard_y(4) $courtyard_y(1)
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    foreach vertex [iterint 1 3] {
	lappend lines [format $formatstr $courtyard_x($vertex) $courtyard_y($vertex) \
			   $courtyard_x([expr $vertex + 1]) $courtyard_y([expr $vertex + 1]) \
			  $params(lt)]
    }
    lappend lines [format $formatstr $courtyard_x(4) $courtyard_y(4) \
		       $courtyard_x(1) $courtyard_y(1) $params(lt)]
    return $lines
}

proc rectangle_boundary_lines {xlength_mm ylength_mm} {
    # Return a list of silkscreen lines describing a rectangle used to
    # show the outer limits of a rectangular component.
    #
    # Arguments:
    #   xlength_mm -- Length in x direction (mm)
    #   ylength_mm -- Length in y direction (mm)
    global params
    puts "Creating rectangle boundary lines"
    # We'll go clockwise around the box starting in the lower left
    # corner of the main body.
    set courtyard_x(1) [expr -double($xlength_mm)/2]
    set courtyard_y(1) [expr double($ylength_mm)/2]
    set courtyard_x(2) $courtyard_x(1)
    set courtyard_y(2) [expr -$courtyard_y(1)]
    set courtyard_x(3) [expr -$courtyard_x(1)]
    set courtyard_y(3) $courtyard_y(2)
    set courtyard_x(4) $courtyard_x(3)
    set courtyard_y(4) $courtyard_y(1)
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    foreach vertex [iterint 1 3] {
	lappend lines [format $formatstr $courtyard_x($vertex) $courtyard_y($vertex) \
			   $courtyard_x([expr $vertex + 1]) $courtyard_y([expr $vertex + 1]) \
			  $params(lt)]
    }
    lappend lines [format $formatstr $courtyard_x(4) $courtyard_y(4) \
		       $courtyard_x(1) $courtyard_y(1) $params(lt)]
    return $lines
}

proc click_boundary_lines {length_mm} {
    # Return a list of silkscreen lines describing the outside of a
    # click board from Mikroe.
    #
    # Arguments:
    #   length_mm -- Length of the board in mm
    global params
    set x(1) [expr (25.4 * 0.5) - $length_mm]
    set y(1) [expr 25.4 * 0.5]
    set x(2) [expr 25.4 * 0.5]
    set y(2) $y(1)
    set x(3) $x(2)
    set y(3) [expr -25.4 * 0.45]
    set x(4) [expr 25.4 * 0.45]
    set y(4) [expr -25.4 * 0.5]
    set x(5) $x(1)
    set y(5) [expr -$y(1)]
    set x(6) $x(1)
    set y(6) $y(1)
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    foreach point [iterint 1 5] {
	set nextpoint [expr $point + 1]
	lappend lines [format $formatstr $x($point) $y($point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    return $lines
}

proc polygon_boundary_lines {point_list} {
    # Return a list of silkscreen lines connecting the supplied points
    #
    # Arguments:
    #   point_list -- List of alternating x_mm and y_mm to connect
    global params
    foreach {x_mm y_mm} $point_list {
	lappend pair_list [list $x_mm $y_mm]
    }
    # Number of pairs from the incoming point list
    set argument_pair_count [llength $pair_list]
    # Add the first pair to the end to make a closed polygon
    lappend pair_list [lindex $pair_list 0]
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    foreach index [iterint 0 $argument_pair_count] {
	set x1_mm [lindex [lindex $pair_list $index] 0]
	set y1_mm [lindex [lindex $pair_list $index] 1]
	set x2_mm [lindex [lindex $pair_list [expr $index + 1]] 0]
	set y2_mm [lindex [lindex $pair_list [expr $index + 1]] 1]
	lappend lines [format $formatstr $x1_mm $y1_mm $x2_mm $y2_mm $params(lt)]
    }
    return $lines
}


proc eple_boundary_lines {} {
    # Return a lists of silkscreen lines describing the outside of the
    # WE-EPLE USB-A connector
    global params
    # Front of the connector starting in lower left corner
    set front_x(1) -6.9
    set front_y(1) 13.1
    set front_x(2) $front_x(1)
    set front_y(2) 4.5
    set front_x(3) [expr -$front_x(1)]
    set front_y(3) $front_y(2)
    set front_x(4) $front_x(3)
    set front_y(4) $front_y(1)
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    foreach {vs ve} [list 1 2 3 4 4 1] {
	lappend lines [format $formatstr $front_x($vs) $front_y($vs) \
			   $front_x($ve) $front_y($ve) \
			   $params(lt)]
    }
    # Back of the connector starting in lower left corner
    set back_x(1) -6.9
    set back_y(1) 1
    set back_x(2) $back_x(1)
    set back_y(2) -1.4
    set back_x(3) [expr -$back_x(1)]
    set back_y(3) $back_y(2)
    set back_x(4) $back_x(3)
    set back_y(4) $back_y(1)
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    foreach {vs ve} [list 1 2 2 3 3 4] {
	lappend lines [format $formatstr $back_x($vs) $back_y($vs) \
			   $back_x($ve) $back_y($ve) \
			   $params(lt)]
    }
    return $lines
}

proc ucl10x10_boundary_lines {} {
    # Return a list of silkscreen lines describing the outside of the
    # UCL capacitor with base dimensions of 10 x 10 mm
    global params
    # Base is square
    set base_length_mm 10.3
    # Base has a gap in the middle for the pins
    set base_gap_mm 3.0
    # Wing width divided by polarity feature width.  Reduce this to
    # bevel more of the edge.
    set bevel_fraction 2
    # Wing width derived from length and gap
    set wing_width_mm [expr ($base_length_mm - $base_gap_mm)/2]
    # Length of polarity mark
    set polarity_length_mm [expr double($wing_width_mm)/$bevel_fraction]
    # We'll go clockwise around the box starting in the lower left corner
    set x(1) [expr -double($base_length_mm)/2 + double($wing_width_mm)/$bevel_fraction]
    set y(1) [expr double($base_length_mm)/2]
    set x(2) [expr -double($base_length_mm)/2]
    set y(2) [expr double($base_length_mm)/2 - double($wing_width_mm)/$bevel_fraction]
    set x(3) $x(2)
    set y(3) [expr double($base_gap_mm)/2]
    set x(4) $x(2)
    set y(4) -$y(3)
    set x(5) $x(2)
    set y(5) -$y(2)
    set x(6) $x(1)
    set y(6) -$y(1)
    set x(7) [expr double($base_length_mm)/2]
    set y(7) $y(6)
    set x(8) $x(7)
    set y(8) $y(4)
    set x(9) $x(8)
    set y(9) $y(3)
    set x(10) $x(9)
    set y(10) $y(1)
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    foreach point [iterint 1 2] {
	set nextpoint [expr $point + 1]
	lappend lines [format $formatstr $x($point) $y($point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    foreach point [iterint 4 4] {
	set nextpoint [expr $point + 1]
	lappend lines [format $formatstr $x($point) $y($point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    lappend lines [format $formatstr $x(9) $y(9) $x(10) $y(10) $params(lt)]
    lappend lines [format $formatstr $x(10) $y(10) $x(1) $y(1) $params(lt)]
    # Add polarity mark
    set x(p1) [expr -double($base_length_mm)/2 - double($polarity_length_mm)/2]
    set y(p1) [expr double($base_length_mm)/2]
    set x(p2) [expr $x(p1) + $polarity_length_mm]
    set y(p2) $y(p1)
    set x(p3) [expr -double($base_length_mm)/2]
    set y(p3) [expr double($base_length_mm)/2 - double($polarity_length_mm)/2]
    set x(p4) $x(p3)
    set y(p4) [expr double($base_length_mm)/2 + double($polarity_length_mm)/2]
    foreach point [iterint 1 1] {
	set nextpoint p[expr $point + 1]
	lappend lines [format $formatstr $x(p$point) $y(p$point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    foreach point [iterint 3 1] {
	set nextpoint p[expr $point + 1]
	lappend lines [format $formatstr $x(p$point) $y(p$point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    return $lines
}

proc lmzfl_boundary_lines {} {
    # Return a list of silkscreen lines describing the outside of the
    # LMZFL connector
    global params
    # Box with pins at the corners
    set pinbox_width_mm [expr ($params(n)/2 - 1) * 5.0]
    set pinbox_height_mm 5.0
    # LMZFL has a feature on its "pin 1" edge.  Make a box to show how the connector
    # should be inserted.
    set polbox_width_mm 1.0
    # We'll go clockwise around the main box starting in the lower
    # left corner
    set x1 [expr -double($pinbox_width_mm)/2 - 3.9]
    set y1 [expr double($pinbox_height_mm)/2 + 5.0]
    set x2 $x1
    set y2 [expr -double($pinbox_height_mm)/2 - 5.0]
    set x3 [expr double($pinbox_width_mm)/2 + 4.1]
    set y3 $y2
    set x4 $x3
    set y4 $y1
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    lappend lines [format $formatstr $x1 $y1 $x2 $y2 $params(lt)]
    lappend lines [format $formatstr $x2 $y2 $x3 $y3 $params(lt)]
    lappend lines [format $formatstr $x3 $y3 $x4 $y4 $params(lt)]
    lappend lines [format $formatstr $x4 $y4 $x1 $y1 $params(lt)]
    # Now make the polarity line
    set xp [expr $x1 + $polbox_width_mm]
    lappend lines [format $formatstr $xp $y1 $xp $y2 $params(lt)]
    return $lines
}

proc r78e_boundary_lines {} {
    # Return a list of silkscreen lines describing the outside of the
    # R-78E DC/DC converter
    global params
    # We'll go clockwise around the box starting in the lower left corner
    set x1 -5.75
    set y1 2.0
    set x2 $x1
    set y2 -6.5
    set x3 [expr -$x1]
    set y3 $y2
    set x4 $x3
    set y4 $y1
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    lappend lines [format $formatstr $x1 $y1 $x2 $y2 $params(lt)]
    lappend lines [format $formatstr $x2 $y2 $x3 $y3 $params(lt)]
    lappend lines [format $formatstr $x3 $y3 $x4 $y4 $params(lt)]
    lappend lines [format $formatstr $x4 $y4 $x1 $y1 $params(lt)]
    return $lines
}

proc dil_assembly_lines {pad_line_list} {
    # Return a list of silkscreen lines for the area between pad rows
    #
    # Arguments:
    #   pad_line_list -- List of pad lines created by dil_pad_line
    global params
    # Offset from the lower left corner to the dot
    set dot_offset_mm 0.75
    set dot_diameter_mm 1
    lassign [pad_inner_extremes $pad_line_list] xmin ymax xmax ymin
    # Actual copper boundaries will have a 1/2 thickness value added
    # to them.  For DIL pads, all pads are the same width.
    set padline [lindex $pad_line_list 0]
    set pad_width_mm [dim_to_num [lindex [split $padline] 6]]
    set bbox_xmin [expr $xmin - double($pad_width_mm)/2]
    set bbox_xmax [expr $xmax + double($pad_width_mm)/2]
    set bbox_ymin [expr $ymin + double($pad_width_mm)/2]
    set bbox_ymax [expr $ymax - double($pad_width_mm)/2]
    # We'll go clockwise around the courtyard starting in the lower left corner.
    set x(1) [expr $bbox_xmin - $params(cs)/2]
    set y(1) [expr $bbox_ymax - 0.2]
    set x(2) $x(1)
    set y(2) [expr $bbox_ymin + 0.2]
    set x(3) [expr $bbox_xmax + $params(cs)/2]
    set y(3) $y(2)
    set x(4) $x(3)
    set y(4) $y(1)
    set x(5) $x(1)
    set y(5) $y(1)
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    foreach point [iterint 1 4] {
	set nextpoint [expr $point + 1]
	lappend lines [format $formatstr $x($point) $y($point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    # Define the pin 1 dot inside the body
    set xdot [expr $x(1) + $dot_offset_mm]
    set ydot [expr $y(1) - $dot_offset_mm]
    set formatstr "ElementArc\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3f %0.3f %0.3fmm \]"
    lappend lines [format $formatstr $xdot $ydot \
		       [expr double($dot_diameter_mm)/4] \
		       [expr double($dot_diameter_mm)/4] \
		       0 359 \
		       [expr double($dot_diameter_mm)/2]]
    # Define the pin 1 dot outside the pins
    set outer_dot_diameter_mm 0.5
    set outer_dot_offset_mm 0.2
    set pad_1_line [lindex $pad_line_list 0]
    set pad_1_x [dim_to_num [lindex [split $pad_1_line] 2]]
    set pad_1_y1 [dim_to_num [lindex [split $pad_1_line] 3]]
    set pad_1_y2 [dim_to_num [lindex [split $pad_1_line] 5]]
    set xdot [expr $pad_1_x - double($pad_width_mm)/2 - double($outer_dot_diameter_mm)/2 - $outer_dot_offset_mm]
    set ydot [expr ($pad_1_y1 + $pad_1_y2)/2]
    set dot_diameter_mm 1
    lappend lines [format $formatstr $xdot $ydot \
		       [expr double($outer_dot_diameter_mm)/4] \
		       [expr double($outer_dot_diameter_mm)/4] \
		       0 359 \
		       [expr double($outer_dot_diameter_mm)/2]]
    return $lines
}

proc tstsm_assembly_lines {pad_line_list} {
    # Return a line for the pin 1 dot
    #
    # Arguments:
    #   pad_line_list -- List of pad lines
    global params
    # Offset from the lower left corner to the dot
    set dot_offset_x_mm 0
    set dot_offset_y_mm 1.5
    set dot_diameter_mm 1
    lassign [pad_outer_extremes $pad_line_list] xmin ymax xmax ymin
    # Define the pin 1 dot
    set xdot [expr $xmin - $dot_offset_x_mm]
    set ydot [expr $ymax + $dot_offset_y_mm]
    set formatstr "ElementArc\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3f %0.3f %0.3fmm \]"
    lappend lines [format $formatstr $xdot $ydot \
		       [expr double($dot_diameter_mm)/4] \
		       [expr double($dot_diameter_mm)/4] \
		       0 359 \
		       [expr double($dot_diameter_mm)/2]]
    return $lines
}

proc astar_328pb_assembly_lines {} {
    # Return a line for the pin 1 dot on the A-star 328pb footprint
    global params
    # Offset from pin 1 to the dot
    set dot_offset_x_mm 0
    set dot_offset_y_mm 2.0
    set dot_diameter_mm 1
    set pin1_x_mm [expr -0.5 * 25.4]
    set pin1_y_mm [expr (0.35 - 0.05) * 25.4]
    # Set the dot position
    set xdot [expr $pin1_x_mm - $dot_offset_x_mm]
    set ydot [expr $pin1_y_mm + $dot_offset_y_mm]
    set formatstr "ElementArc\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3f %0.3f %0.3fmm \]"
    lappend lines [format $formatstr $xdot $ydot \
		       [expr double($dot_diameter_mm)/4] \
		       [expr double($dot_diameter_mm)/4] \
		       0 359 \
		       [expr double($dot_diameter_mm)/2]]
    return $lines
}

proc diode_assembly_lines {offset length} {
    # Return a set of lines for the offset diode drawing
    #
    # Arguments:
    #   offset -- Separation from chip center (mm)
    #   length -- Length scale for diode (mm)
    global params
    # Anode wire
    set x(1) [expr double($length)/2]
    set y(1) [expr -$offset]
    set x(2) [expr $x(1) + $length]
    set y(2) $y(1)
    set formatstr "ElementLine\[ %0.3fmm %0.3fmm %0.3fmm %0.3fmm %0.3fmm \]"
    foreach point [iterint 1 1] {
	set nextpoint [expr $point + 1]
	lappend lines [format $formatstr $x($point) $y($point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    # Triangle
    set triangle_scale 2
    set x(1) [expr double($length)/2]
    set y(1) [expr -$offset - double($length)/$triangle_scale]
    set x(2) $x(1)
    set y(2) [expr -$offset + double($length)/$triangle_scale]
    set x(3) [expr -$x(1)]
    set y(3) [expr -$offset]
    set x(4) $x(1)
    set y(4) $y(1)
    foreach point [iterint 1 3] {
	set nextpoint [expr $point + 1]
	lappend lines [format $formatstr $x($point) $y($point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    # Cathode
    set x(1) [expr -double($length)/2]
    set y(1) [expr -$offset - double($length)/$triangle_scale]
    set x(2) $x(1)
    set y(2) [expr -$offset + double($length)/$triangle_scale]
    foreach point [iterint 1 1] {
	set nextpoint [expr $point + 1]
	lappend lines [format $formatstr $x($point) $y($point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    # Cathode wire
    set x(1) [expr -double($length)/2]
    set y(1) [expr -$offset]
    set x(2) [expr $x(1) - $length]
    set y(2) $y(1)
    foreach point [iterint 1 1] {
	set nextpoint [expr $point + 1]
	lappend lines [format $formatstr $x($point) $y($point) $x($nextpoint) $y($nextpoint) $params(lt)]
    }
    return $lines
}

proc write_lines {file_pointer line_list} {
    foreach line $line_list {
	if {[string length $line] > 0} {
	    # Refuse to write blank lines
	    puts $file_pointer $line
	}
    }
}

if [string match "dda" $params(t)] {
    # TI's DDA package dimensions in mm.  These packages have tip-to-tip
    # of 6mm, and TI calls for an outer pad-to-pad dimension of
    # 7mm.  This leaves 0.5mm of visible pad length once the part
    # is placed.
    set inner_pad_to_pad_mm 3.85
    set outer_pad_to_pad_mm 6.95
    set pitch_mm 1.27
    set width_mm 0.6
    #################### PowerPAD under the part #####################
    set slug_x_mm 4.9
    set slug_y_mm 2.95
    set slug_drill_mm 0.2
    # Number of drills in x dimension
    set slug_via_x_n 4
    # Number of drills in y dimension
    set slug_via_y_n 2
    # Via-to-via spacing
    set slug_via_pitch_mm 1.3
    # Mask opening
    set slug_mask_x_mm 3.1
    set slug_mask_y_mm 2.6
}

if [string match "db" $params(t)] {
    # TI's DB package dimensions in mm.  These packages have
    # tip-to-tip of 8.1mm, and TI calls for an outer pad-to-pad
    # dimension of 9.1mm.  This leaves 0.5mm of visible pad length
    # once the part is placed.
    set inner_pad_to_pad_mm 5.584
    set outer_pad_to_pad_mm 9.116
    set pitch_mm 0.650
    set width_mm 0.451
}

if [string match "dca" $params(t)] {
    # TI's DCA package dimensions in mm.  These packages have
    # tip-to-tip of 8.1mm, and TI calls for an outer pad-to-pad
    # dimension of 9.1mm.  This leaves 0.5mm of visible pad length
    # once the part is placed.
    set inner_pad_to_pad_mm 5.95
    set outer_pad_to_pad_mm 9.1
    set pitch_mm 0.5
    set width_mm 0.3
    #################### PowerPAD under the part #####################
    set slug_x_mm 12.5
    set slug_y_mm 5.1
    set slug_drill_mm 0.2
    # Number of drills in x dimension
    set slug_via_x_n 10
    # Number of drills in y dimension
    set slug_via_y_n 4
    # Via-to-via spacing
    set slug_via_pitch_mm 1.3
    # Mask opening
    set slug_mask_x_mm 6.4
    set slug_mask_y_mm 3.6
}

if [string match "dgk" $params(t)] {
    # TI's DGK package dimensions in mm.  These packages have
    # tip-to-tip of 5mm, and TI calls for an outer pad-to-pad
    # dimension of 5.85mm.  This leaves about 0.5mm of visible pad
    # length once the part is placed.
    set inner_pad_to_pad_mm 2.95
    set outer_pad_to_pad_mm 5.85
    set pitch_mm 0.65
    set width_mm 0.45
}

if [string match "dcu" $params(t)] {
    # TI's DCU package dimensions in mm.  These packages have
    # tip-to-tip of 3.1mm, and TI calls for an outer pad-to-pad
    # dimension of 3.85mm.  This leaves about 0.4mm of visible pad
    # length once the part is placed.
    set inner_pad_to_pad_mm 2.35
    set outer_pad_to_pad_mm 3.85
    set pitch_mm 0.5
    # Width (short dimension) of individual pads
    set width_mm 0.25
}

if [string match "cts625" $params(t)] {
    # Model 625 oscillators from CTS.
    set params(n) 4
    set inner_pad_to_pad_mm 0.5
    set outer_pad_to_pad_mm 2.2
    set pitch_mm 1.7
    # Pad width -- dimension along line of pads on one side of part
    set width_mm 1.0
}

if [string match "dscdfn" $params(t)] {
    # Model DSC60xx oscillators from Microchip in DFN packages
    set params(n) 4
    set inner_pad_to_pad_mm 1.2
    set outer_pad_to_pad_mm 3.0
    set pitch_mm 1.9
    # Pad width -- dimension along line of pads on one side of part
    set width_mm 1.0
}

if [string match "pts645" $params(t)] {
    # PTS645 6mm tactile switches from C&K.  See part 21-12 for more details.
    set params(n) 4
    set inner_pad_to_pad_mm 3.2
    set outer_pad_to_pad_mm 5.8
    set pitch_mm 7.95
    # Pad width -- dimension along line of pads on one side of part
    set width_mm 1.55
}

if [string match "musbr" $params(t)] {
    # PCB-mount USB-C connector from Amphenol.  Amphenol recommends a
    # drill diameter of .53mm, and a ring diameter of 0.93mm.
    #
    # The pin pitch isn't really relevant here, as it varies all over the connector.
    #
    # Pin drill diameter (mm)
    set pin_drill_mm 0.53
    # Width of the pad the pin is drilled in (mm)
    set pin_thickness_mm 0.93
    # List of x (mm), y (mm), name data for each pin
    set pin_location_list [list 5.35 1.325 B1 \
			       -5.35 1.325 B12 \
			       4.25 1.325 B2 \
			       -4.25 1.325 B11 \
			       3.15 1.325 B3 \
			       -3.15 1.325 B10 \
			       2.05 1.325 B4 \
			       -2.05 1.325 B9 \
			       1.30 0.525 B5 \
			       -1.30 0.525 B8 \
			       0.55 1.325 B6 \
			       -0.55 1.325 B7 \
			       5.725 -1.325 A12 \
			       -5.725 -1.325 A1 \
			       4.625 -1.325 A11 \
			       -4.625 -1.325 A2 \
			       3.525 -1.325 A10 \
			       -3.525 -1.325 A3 \
			       2.425 -1.325 A9 \
			       -2.425 -1.325 A4 \
			       1.675 -0.525 A8 \
			       -1.675 -0.525 A5 \
			       0.925 -1.325 A7 \
			       -0.925 -1.325 A6 \
			       -4.95 0 G1 \
			       4.95 0 G6 \
			       -3.85 0 G2 \
			       3.85 0 G5 \
			       -2.75 0 G3 \
			       2.75 0 G4
			  ]
    # Mounting hole drill diameter (mm)
    #
    # The mounting tabs are 2.65mm long.  Oversizing the hole by 10mils gives me
    set mthole_drill_mm [expr 2.65 + 0.254]

    # Width of the pad the mounting hole is drilled in (mm)
    set mthole_thickness_mm [expr $mthole_drill_mm + 1]

    # List of x (mm), y (mm), name data for each mounting hole
    set mthole_location_list [list 8.9 0 mthole \
				  -8.9 0 mthole]
}

if {"$params(t)" eq "anano"} {
    # Arduino nano
    #
    # The Arduino nano is a PCB mounted on what look like Samtec TSW 0.1-inch pitch SIL headers.
    #
    # Samtec's TST or TSW series of pin headers have pins 0.64mm
    # square, and a good drill diameter is 1.02mm.
    set pin_drill_mm 1.02

    # The pitch is 0.1 inches
    set pitch_mm 2.54

    # There is 0.6 inches between rows of pins
    set row_pin_to_pin_mm 15.240

    # Width of the annular ring around the pin
    set pin_ring_mm [expr 0.01 * 25.4]

    # Always has 30 pins
    set params(n) 30
}

if [string match "eple" $params(t)] {
    # PCB-mount USB-A connector from Wurth.  Wurth recommends a
    # pin drill diameter of .92mm.
    #
    # Pin drill diameter (mm)
    set pin_drill_mm 0.92
    # Width of the pad the pin is drilled in (mm)
    set pin_thickness_mm [expr $pin_drill_mm + 0.5]
    # List of x (mm), y (mm), name data for each pin
    set pin_location_list [list -3.5 0 1 \
			       -1 0 2 \
			       1 0 3 \
			       3.5 0 4
			  ]
    # Mounting hole drill diameter (mm)
    #
    # Wurth recommends 2.3mm
    set mthole_drill_mm 2.3

    # Width of the pad the mounting hole is drilled in (mm)
    set mthole_thickness_mm [expr $mthole_drill_mm + 1]

    # List of x (mm), y (mm), name data for each mounting hole
    set mthole_location_list [list -6.57 2.7 m \
				  6.57 2.7 m]
}

if {[string match "shf" $params(t)]} {
    # Samtec's SHF series of pin headers.  These pins are
    # 0.41mm square, and a good drill diameter is 0.74mm.
    set pin_drill_mm 0.74
    # Width of the annular ring around the pin.  A 7-mil annular ring
    # will give a 7-mil separation between rings
    set pin_ring_mm [expr 0.007 * 25.4]
    set pitch_mm 1.270
}

if [string match "lmzfl" $params(t)] {
    # Weidmuller LMZFL terminal blocks.  Weidmuller recommends a 1.3mm
    # drill.
    set pin_drill_mm 1.3
    # Width of the annular ring around the pin
    set pin_ring_mm 0.5
    set pitch_mm 5.0
}

if [string match "r78e" $params(t)] {
    # Recom's R-78E series DC/DC converters.  These pins are 0.7mm
    # wide, and the recommended drill diameter is 1.0mm.
    set pin_drill_mm 1.0
    # Width of the annular ring around the pin
    set pin_ring_mm 0.254
    set pitch_mm 2.54
    # Always has 3 pins
    set params(n) 3
}

if [string match "0603r" $params(t)] {
    # EIA 0603 chip resistor
    #
    # See Vishay's recommendations, document number 28745
    set chip(gap_mm) 0.5
    set chip(length_mm) 0.95
    set chip(width_mm) 0.95
}

if [string match "1206r" $params(t)] {
    # EIA 1206 chip resistor
    #
    # See Vishay's recommendations, document number 28745
    # Number of pads is always 2
    set params(n) 2
    # Gap between pads
    set chip(gap_mm) 1.5
    # Length of each pad along the long axis
    set chip(length_mm) 1.25
    set chip(width_mm) 1.75
}

if [string match "0603c" $params(t)] {
    # EIA 0603 chip capacitor
    #
    # See KEMET's recommendations and part 11-25
    # Number of pads is always 2
    set params(n) 2
    # Gap between pads
    set chip(gap_mm) 0.65
    # Length of each pad along the long axis
    set chip(length_mm) 0.95
    set chip(width_mm) 1.0
}

if [string match "1206c" $params(t)] {
    # EIA 1206 chip capacitor
    #
    # See KEMET's recommendations and part 11-25
    # Number of pads is always 2
    set params(n) 2
    # Gap between pads
    set chip(gap_mm) 1.85
    # Length of each pad along the long axis
    set chip(length_mm) 1.15
    set chip(width_mm) 1.8
}

if [string match "0603c" $params(t)] {
    # EIA 0603 chip capacitor
    #
    # See KEMET's recommendations and part 11-25
    set chip(gap_mm) 0.65
    set chip(length_mm) 0.95
    set chip(width_mm) 1.0
}

if [string match "1210c" $params(t)] {
    # EIA 1210 chip capacitor
    #
    # See KEMET's recommendations and part 11-23
    set chip(gap_mm) 1.85
    set chip(length_mm) 1.15
    set chip(width_mm) 2.7
}

if [string match "1612b" $params(t)] {
    # 1612 ferrite bead
    #
    # See Wurth's recommendations and part 10-10
    #
    # Number of pads is always 2
    set params(n) 2
    # Separation between pads (floating point mm)
    set chip(gap_mm) 2.1
    # Single pad length along long axis of part (floating point mm)
    set chip(length_mm) 3.25
    # Single pad width along short axis of part (floating point mm)
    set chip(width_mm) 4.1
}

if [string match "0805l" $params(t)] {
    # 0805 inductor
    #
    # See TDK's recommendations and part 10-12
    #
    # Number of pads is always 2
    set params(n) 2
    # Separation between pads (floating point mm)
    set chip(gap_mm) 1.0
    # Single pad length along long axis of part (floating point mm)
    set chip(length_mm) 0.8
    # Single pad width along short axis of part (floating point mm)
    set chip(width_mm) 1.2
}

if [string match "ucl10x10" $params(t)] {
    # 10x10 UCL capacitor from Nichicon
    #
    # See Nichicon's recommendations for Alumninum Electrolytic Capacitors
    #
    # Number of pads is always 2
    set params(n) 2
    # Separation between pads (floating point mm)
    set chip(gap_mm) 4.0
    # Single pad length along long axis of part (floating point mm)
    set chip(length_mm) 4.0
    # Single pad width along short axis of part (floating point mm)
    set chip(width_mm) 2.5
}

if [string match "4x4l" $params(t)] {
    # 4mm x 4mm inductor from Wurth
    #
    # See Wurth's recommendations and part 10-13
    #
    # Number of pads is always 2
    set params(n) 2
    # Separation between pads (floating point mm)
    set chip(gap_mm) 1.55
    # Single pad length along long axis of part (floating point mm)
    set chip(length_mm) 1.5
    # Single pad width along short axis of part (floating point mm)
    set chip(width_mm) 3.6
}

if [string match "8x8l" $params(t)] {
    # 8mm x 8mm inductor from Wurth
    #
    # See Wurth's recommendations and part 10-14
    #
    # Number of pads is always 2
    set params(n) 2
    # Separation between pads (floating point mm)
    set chip(gap_mm) 3.8
    # Single pad length along long axis of part (floating point mm)
    set chip(length_mm) 2.4
    # Single pad width along short axis of part (floating point mm)
    set chip(width_mm) 7.5
}

if [string match "smad" $params(t)] {
    # SMA-size diode
    #
    # See SMAJ13A datasheet
    #
    # Number of pads is always 2
    set params(n) 2
    # Separation between pads (floating point mm)
    set chip(gap_mm) 2.3
    # Single pad length along long axis of part (floating point mm)
    set chip(length_mm) 2.1
    # Single pad width along short axis of part (floating point mm)
    set chip(width_mm) 2.6
}

if [string match "snapspace" $params(t)] {
    # Keystone snap-in spacers
    #
    # See part 0-35
    #
    # Number of pins is always 1
    set params(n) 1
    # Mounting hole drill
    set mthole(drill_mm) 4.75
    # Standoff body diameter
    set mthole(od_mm) 6.6

    set mthole(plated) false
}

# Specify footprint dimensions and sometimes number of pins
switch -exact $params(t) {
    "tst" -
    "tsws" -
    "tswsra" {
	# Pin headers
	#
	# Samtec's TST or TSW series of pin headers have pins 0.64mm
	# square, and a good drill diameter is 1.02mm.
	set pin_drill_mm 1.02

	# The pitch is 0.1 inches
	set pitch_mm 2.54

	# Width of the annular ring around the pin
	set pin_ring_mm [expr 0.01 * 25.4]	
    }
    "jstph" {
	# JST-PH series shrouded pin headers
	#
	# Pin drill specified to be 0.7mm
	set pin_drill_mm 0.7

	# Pitch is 2mm
	set pitch_mm 2.0

	# Width of annular ring around the pin
	set pin_ring_mm [expr 0.01 * 25.4]
    }
    "xover_click" {
	# Crossover click board from Mikroe
	#
	# These use Samtec TSW pin headers
	set pin_drill_mm 1.02

	# The pitch is 0.1 inches
	set pitch_mm 2.54

	# Width of the annular ring around the pin
	set pin_ring_mm [expr 0.01 * 25.4]

	# There is 0.9 inches between rows of pins
	set row_pin_to_pin_mm [expr 0.9 * 25.4]

	# Length of the click board is variable
	set click_length_mm [expr 2.250 * 25.4]

	# Always has 16 pins
	set params(n) 16
    }
    "mp3_2_click" {
	# MP3 2 click board from Mikroe
	#
	# These use Samtec TSW pin headers
	set pin_drill_mm 1.02

	# The pitch is 0.1 inches
	set pitch_mm 2.54

	# Width of the annular ring around the pin
	set pin_ring_mm [expr 0.01 * 25.4]

	# There is 0.9 inches between rows of pins
	set row_pin_to_pin_mm [expr 0.9 * 25.4]

	# Length of the click board is variable
	set click_length_mm 43.0

	# Always has 16 pins
	set params(n) 16
    }
    "audioamp_2_click" {
	# AudioAmp 2 click board from Mikroe
	#
	# These use Samtec TSW pin headers
	set pin_drill_mm 1.02

	# The pitch is 0.1 inches
	set pitch_mm 2.54

	# Width of the annular ring around the pin
	set pin_ring_mm [expr 0.01 * 25.4]

	# There is 0.9 inches between rows of pins
	set row_pin_to_pin_mm [expr 0.9 * 25.4]

	# Length of the click board is variable
	set click_length_mm 57.2

	# Always has 16 pins
	set params(n) 16
    }
    "apt1608" {
	# APT1608 flat-top chip LEDs
	#
	# See Kingbright's datasheet
	set chip(gap_mm) 0.85
	set chip(length_mm) 0.8
	set chip(width_mm) 0.8
    }
    "apt3216" {
	# APT3216 flat-top chip LEDs
	#
	# See Kingbright's datasheet

	# Always 2 pads
	set params(n) 2

	# Separation between pads (floating point mm)
	set chip(gap_mm) 2.0

	# Single pad length along long axis of part (floating point mm)
	set chip(length_mm) 1.75

	# Single pad width along short axis of part (floating point mm)
	set chip(width_mm) 1.5
    }
    "smcw8" {
	# Wurth WL-SMCW 0805 LEDs
	#
	# See Wurth's datasheet, 150080GS75000 for example

	# Always 2 pads
	set params(n) 2

	# Separation between pads (floating point mm)
	set chip(gap_mm) 1.0

	# Single pad length along long axis of part (floating point mm)
	set chip(length_mm) 1.1

	# Single pad width along short axis of part (floating point mm)
	set chip(width_mm) 1.2

    }
    "tstsm" {
	# Surface-mount HTST headers from Samtec
	#
	# See Samtec's HTST surface-mount footprint guide.  These
	# connectors have tip-to-tip of 11.18mm, and Samtec calls for
	# an outer pad-to-pad dimension of 11.94mm.
	set inner_pad_to_pad_mm 1.78
	set outer_pad_to_pad_mm 11.94
	set pitch_mm 2.54
	# Width (short dimension) of individual pads
	set width_mm 1.27
    }
    "6-32p" {
	# Plated mounting hole for 6-32 screw

	# Always 1 pin
	set params(n) 1

	# Mounting hole drill.  A free-fit hole for #6 is 3.8mm
	set mthole(drill_mm) 3.8

	# Circle courtyard diameter.  The head diameter for #6 is .226
	# inches.  Add about 0.02 to that for free-fit wandering.
	set mthole(courtyard_mm) 6.35

	# Annular ring thickness (mm)
	set mthole(ring_mm) 1

	set mthole(plated) true
    }
    "m7p" {
	# Plated mounting hole for 6-32 screw

	# Always 1 pin
	set params(n) 1

	# Mounting hole drill.  A free-fit hole for M7 is 7.7mm.  A
	# close fit is 7.4mm.
	set mthole(drill_mm) 7.5

	# Circle courtyard diameter.  The flat-to-flat is 10mm for
	# small M7 nuts, and corner-to-corner is 11.05.  10mm driver
	# diamters are 14.25mm in diamter.
	set mthole(courtyard_mm) 14.25

	# Annular ring thickness (mm)
	set mthole(ring_mm) 1

	set mthole(plated) true
    }
    "d" {
	# TI's D package dimensions in mm.  These packages have
	# tip-to-tip of 6mm, and TI calls for an outer pad-to-pad
	# dimension of 7.468mm in their SNOA293B application note.
	# This leaves 0.75mm of visible pad length once the part is
	# placed.
	#
	# Note that the SN74HC595 datasheet calls for different
	# footprint dimensions for the same footprint.  I think the
	# SNOA293B has JEDEC guidelines, while the datasheet has a
	# more compact footprint.
	set inner_pad_to_pad_mm 2.388
	set outer_pad_to_pad_mm 7.468
	set pitch_mm 1.27
	set pad_width_mm 0.711
    }
    "dbz" {
	# TI's DBZ package dimensions in mm.  These packages have
	# tip-to-tip of 2.4mm, and TI calls for an outer pad-to-pad
	# dimension of 3.4mm.  This leaves about 0.5mm of visible pad
	# length once the part is placed.
	#
	# This package always has 3 pins.
	set params(n) 3
	set inner_pad_to_pad_mm 0.8
	set outer_pad_to_pad_mm 3.4
	# The pitch here is half the distance between the two pins on the
	# same side.  The idea is that this side is "missing" a pin.
	set pitch_mm 0.95
	# Width (short dimension) of individual pads
	set pad_width_mm 0.6

	set body_length_mm 2.9
	set body_width_mm 1.3
    }
    "dbv" {
	# TI's DBV package dimensions in mm.  These packages have
	# tip-to-tip of 3.0mm, and TI calls for an outer pad-to-pad
	# dimension of 3.7mm.  This leaves about 0.4mm of visible pad
	# length once the part is placed.
	#
	# This package always has 6 pins.
	set params(n) 6
	set inner_pad_to_pad_mm 1.5
	set outer_pad_to_pad_mm 3.7
	# The pitch here is half the distance between the two pins on the
	# same side.  The idea is that this side is "missing" a pin.
	set pitch_mm 0.95
	# Width (short dimension) of individual pads
	set pad_width_mm 0.6

	set body_length_mm 2.9
	set body_width_mm 1.6
    }
    "sot323" {
	# 3-pin device from Diodes Inc.  These packages have
	# tip-to-tip of 2.1mm, and Diodes calls for an outer
	# pad-to-pad dimension of 2.5mm.  This leaves about 0.2mm
	# visible pad length once the part is placed.  I usually like
	# 0.5mm, so make the outer pad-to-pad 3.1mm.
	#
	# This package always has 3 pins.
	set params(n) 3
	set inner_pad_to_pad_mm 1.3
	set outer_pad_to_pad_mm 3.1
	# The pitch here is half the distance between the two pins on the
	# same side.  The idea is that this side is "missing" a pin.
	set pitch_mm 0.65
	# Width (short dimension) of individual pads
	set pad_width_mm 0.6

	set body_length_mm 2.15
	set body_width_mm 1.3
    }
    "smbth" {
	# Through-hole SMB coaxial connector.  See Pomona 72997.
	#
	# This package always has 5 pins.
	set params(n) 5

	# The pins are 1.2mm in diameter.  I'll make the holes 0.12mm
	# larger than the pins.
	set pin_drill_mm 1.32

	# Width of the pad the pin is drilled in (mm)
	set pin_thickness_mm 2.5

	# List of x (mm), y (mm), name data for each pin
	set pin_location_list [list 0 0 1 \
				   2.55 2.55 2 \
				   -2.55 2.55 3 \
				   -2.55 -2.55 4 \
				   2.55 -2.55 5]
    }
    "ces-571423" {
	# CUI loudspeaker, part CES-5714-28PM
	#
	# This is just two holes for solder-in-place standoffs and a silkscreen outline
	set params(n) 2

	# The recommended drill for Wurth 9774025360R spacers is
	# 4.4mm, but this is non-plated.  I want to plate, so I'll
	# oversize the hole by about 10mils.
	set pin_drill_mm 4.6

	# The recommended pad thickness is 7.4mm, but I want it a bit
	# bigger for more mechanical adhesion.
	set pin_thickness_mm 11.0

	# List of x (mm), y (mm), name data for each pin
	set pin_location_list [list -36.25 5.4 "none" \
				  36.25 -5.4 "none"]
    }
    "sj5" {
	# Panel-mount 3.5mm jack.  See CUI SJ5-43502PM datasheet.
	#
	# This is not meant to be PC-mounted, and I'll skip pin 4.
	set params(n) 3

	# Solder lugs for pins 1 and 2 are 2mm wide.  Pin 3 is 2.2mm wide.
	#
	# Pin definition dictionary: <pin name>: drill size, pin thickness, x position (mm), y position (mm)

	# Pin 1 is a 2mm tab.  Make the hole 0.1mm bigger
	dict set pin_definition_dict "1" drill_mm 2.1
	dict set pin_definition_dict "1" thickness_mm 3.1
	dict set pin_definition_dict "1" x_mm 0
	dict set pin_definition_dict "1" y_mm 2.6

	# Pin 2 is also a 2mm tab.
	dict set pin_definition_dict "2" drill_mm 2.1
	dict set pin_definition_dict "2" thickness_mm 3.1
	dict set pin_definition_dict "2" x_mm 0
	dict set pin_definition_dict "2" y_mm -2.6

	# Pin 3 is 2.2mm wide
	dict set pin_definition_dict "3" drill_mm 2.3
	dict set pin_definition_dict "3" thickness_mm 3.3
	dict set pin_definition_dict "3" x_mm 2.5
	dict set pin_definition_dict "3" y_mm 0
    }
    "astar_328pb" {
	# A-Star 328PB board from Pololu
	#
	# The A-Star uses Samtec TSW 0.1-inch pitch headers on three sides
	#
	# Samtec's TST or TSW series of pin headers have pins 0.64mm
	# square, and a good drill diameter is 1.02mm.
	set pin_drill_mm 1.02

	# Width of the pad the pin is drilled in (mm)
	set pin_thickness_mm [expr $pin_drill_mm + 0.02 * 25.4]

	# List of x (mm), y (mm), name data for each pin
	set pin_location_list [list [expr -0.5 * 25.4] [expr (0.35 - 0.05) * 25.4] 1 \
				   [expr -0.4 * 25.4] [expr (0.35 - 0.05) * 25.4] 2 \
				   [expr -0.3 * 25.4] [expr (0.35 - 0.05) * 25.4] 3 \
				   [expr -0.2 * 25.4] [expr (0.35 - 0.05) * 25.4] 4 \
				   [expr -0.1 * 25.4] [expr (0.35 - 0.05) * 25.4] 5 \
				   [expr -0.0 * 25.4] [expr (0.35 - 0.05) * 25.4] 6 \
				   [expr 0.1 * 25.4] [expr (0.35 - 0.05) * 25.4] 7 \
				   [expr 0.2 * 25.4] [expr (0.35 - 0.05) * 25.4] 8 \
				   [expr 0.3 * 25.4] [expr (0.35 - 0.05) * 25.4] 9 \
				   [expr 0.4 * 25.4] [expr (0.35 - 0.05) * 25.4] 10 \
				   [expr 0.5 * 25.4] [expr (0.35 - 0.05) * 25.4] 11 \
				   [expr 0.6 * 25.4] [expr (0.35 - 0.05) * 25.4] 12 \
				   [expr 0.6 * 25.4] [expr (0.35 - 0.15) * 25.4] 13 \
				   [expr 0.6 * 25.4] [expr (0.35 - 0.25) * 25.4] 14 \
				   [expr 0.6 * 25.4] [expr (0.35 - 0.35) * 25.4] 15 \
				   [expr 0.6 * 25.4] [expr (0.35 - 0.45) * 25.4] 16 \
				   [expr 0.6 * 25.4] [expr (0.35 - 0.55) * 25.4] 17 \
				   [expr 0.6 * 25.4] [expr (0.35 - 0.65) * 25.4] 18 \
				   [expr 0.5 * 25.4] [expr (0.35 - 0.65) * 25.4] 19 \
				   [expr 0.4 * 25.4] [expr (0.35 - 0.65) * 25.4] 20 \
				   [expr 0.3 * 25.4] [expr (0.35 - 0.65) * 25.4] 21 \
				   [expr 0.2 * 25.4] [expr (0.35 - 0.65) * 25.4] 22 \
				   [expr 0.1 * 25.4] [expr (0.35 - 0.65) * 25.4] 23 \
				   [expr 0.0 * 25.4] [expr (0.35 - 0.65) * 25.4] 24 \
				   [expr -0.1 * 25.4] [expr (0.35 - 0.65) * 25.4] 25 \
				   [expr -0.2 * 25.4] [expr (0.35 - 0.65) * 25.4] 26 \
				   [expr -0.3 * 25.4] [expr (0.35 - 0.65) * 25.4] 27 \
				   [expr -0.4 * 25.4] [expr (0.35 - 0.65) * 25.4] 28 \
				   [expr -0.5 * 25.4] [expr (0.35 - 0.65) * 25.4] 29
			      ]
    }
    "maskpad" {
	# Single square pad covered with soldermask.  Pad dimension set by the d parameter.
	set params(n) 1
    }
    "3386f" {
	# Bourns 3386F PCB-mount potentiometers
	#
	# Three pins have 0.51mm round diameters.  Oversize the holes by 0.25mm
	set pin_drill_mm 0.75

	# Width of the pad the pin is drilled in (mm)
	set pin_thickness_mm [expr $pin_drill_mm + 0.02 * 25.4]

	# x-direction body length
	set boundary_xlength_mm 9.53

	# y-direction body length
	set boundary_ylength_mm 9.53

	# List of x (mm), y (mm), name data for each pin
	set pin_location_list [list -2.54 -2.54 1 \
				   0.0 2.54 2 \
				   2.54 -2.54 3
			       ]
    }
    "mta100" {
	# TE's MTA-100 pin headers.  These pins are 0.63mm square, and the
	# recommended drill diameter is 1.02mm.
	set pin_drill_mm 1.02
	# Width of the annular ring around the pin
	set pin_ring_mm 0.254
	set pitch_mm 2.54
    }
}

########################## Main entry point ##########################

if $params(t?) {
    list_known_footprint_types
    exit
}

# Open the output file
try {
    set fp [open $params(o) w]
} trap {} {message optdict} {
    # message will be the human-readable error.  optdict will be the
    # options dictionary present when the command was attempted.
    puts "$message"
    puts [dict get $optdict -errorcode]
    exit
}

# List of lines in the element
set element_list [list]

# Add the pin / pad lines
foreach pin [iterint 1 $params(n)] {
    if {[string match "lmzfl" $params(t)]} {
	# LMZFL terminal block
	lappend pin_line_list [lmzfl_pin_line $pin $params(n) \
				   $pin_drill_mm $pin_ring_mm \
				   $pitch_mm]
    } elseif {[string match "mta100" $params(t)] || \
		  [string match "r78e" $params(t)]} {
	# SIL through-hole package
	lappend pin_line_list [mta_pin_line $pin $params(n) \
				   $pin_drill_mm $pin_ring_mm \
				   $pitch_mm]
    } elseif {[string match "0603?" $params(t)] || \
		  [string match "1210?" $params(t)] || \
		  [string match "1612b" $params(t)] || \
		  [string match "0805l" $params(t)] || \
		  [string match "smad" $params(t)] || \
		  [string match "4x4l" $params(t)] || \
		  [string match "8x8l" $params(t)] || \
		  [string match "ucl10x10" $params(t)]} {
	# 2-pin chip
	lappend pad_line_list [chip_pad_line $pin $chip(gap_mm) \
				  $chip(length_mm) $chip(width_mm)]
    } elseif {[string match "snapspace" $params(t)]} {
	# Single mounting hole
	lappend pin_line_list [unplated_hole 1 $mthole(drill_mm)]
    }
    switch -exact $params(t) {
	"tsws" -
	"tswsra" -
	"jstph" {
	    # Single-row through-hole pin headers
	    lappend pin_line_list [tsws_pin_line $pin $params(n) \
				       $pin_drill_mm $pin_ring_mm \
				       $pitch_mm]
	}
	"xover_click" -
	"mp3_2_click" -
	"audioamp_2_click" -
	"anano" {
	    # Dual inline through-hole IC package (DIP)
	    lappend pin_line_list [dil_pin_line $pin $params(n) $row_pin_to_pin_mm $pin_drill_mm $pin_ring_mm $pitch_mm]
	}
	"tst" -
	"shf" {
	    # Dual-row through-hole pin header
	    lappend pin_line_list [tst_pin_line $pin $params(n) \
				   $pin_drill_mm $pin_ring_mm \
				   $pitch_mm]
	}
	"apt1608" -
	"apt3216" -
	"1206r" -
	"smcw8" -
	"1206c" {
	    # 2-pin chip
	    lappend pad_line_list [chip_pad_line $pin $chip(gap_mm) $chip(length_mm) $chip(width_mm)]
	}
	"db" -
	"dca" -
	"dda" -
	"dgk" -
	"dcu" -
	"dbv" -
	"cts625" -
	"dscdfn" -
	"pts645" -
	"d" {
	    # Dual inline SMT IC pads
	    lappend pad_line_list [dil_pad_line $pin $params(n) \
				       $inner_pad_to_pad_mm $outer_pad_to_pad_mm \
				       $pad_width_mm $pitch_mm]
	}
	"tstsm" {
	    # Dual inline SMT connector pads
	    lappend pad_line_list [dil_connector_pad_line $pin $params(n) \
				       $inner_pad_to_pad_mm $outer_pad_to_pad_mm \
				       $width_mm $pitch_mm]
	}
	"snapspace" -
	"m7p" -
	"6-32p" {
	    # Single mounting hole
	    lappend pin_line_list [single_hole 1 $mthole(drill_mm) $mthole(ring_mm) $mthole(plated)]
	}
	"dbz" -
	"sot323" {
	    lappend pad_line_list [dbz_pad_line $pin $params(n) \
				   $inner_pad_to_pad_mm $outer_pad_to_pad_mm \
				   $pad_width_mm $pitch_mm]
	}
	"squarepad" {
	    # Single square pad
	    
	    lappend pad_line_list [square_pad_line $params(d)]
	}
	"maskpad" {
	    # Single square masked pad
	    lappend pad_line_list [masked_square_pad_line $params(d)]
	}
    }
}

if [info exists pin_location_list] {
    # Array of pins specified in a list
    foreach {x y name} $pin_location_list {
	lappend pin_line_list [list_pin_line $x $y $name $pin_drill_mm $pin_thickness_mm]
    }
}

if [info exists pin_definition_dict] {
    # Array of pins specified in a dictionary
    foreach pin_name [dict keys $pin_definition_dict] {
	set x [dict get $pin_definition_dict $pin_name x_mm]
	set y [dict get $pin_definition_dict $pin_name y_mm]
	set pin_drill_mm [dict get $pin_definition_dict $pin_name drill_mm]
	set pin_thickness_mm [dict get $pin_definition_dict $pin_name thickness_mm]
	lappend pin_line_list [list_pin_line $x $y $pin_name $pin_drill_mm $pin_thickness_mm]
    }
}

if [info exists mthole_location_list] {
    # Array of pins specified in a list
    foreach {x y name} $mthole_location_list {
	lappend pin_line_list [list_pin_line $x $y $name $mthole_drill_mm $mthole_thickness_mm]
    }
}

# Add the center thermal slug pad if necessary
if {[string match "dca" $params(t)] || \
    [string match "dda" $params(t)]} {
    # Add the center pad line
    set slug_list [slug_lines [expr $params(n) + 1] \
		       $slug_x_mm $slug_y_mm \
		       $slug_mask_x_mm $slug_mask_y_mm]
    # Add the slug vias if necessary
    try {
	set via_list [slug_vias [expr $params(n) + 1] \
			  $slug_via_x_n $slug_via_y_n \
			  $slug_drill_mm $slug_via_pitch_mm]
	foreach line $via_list {
	    lappend slug_list $line
	}
    } trap {} {msg o} {
	puts $msg
	puts "No slug vias found"
    }
}

# Add mounting holes if necessary
if {[string match "shf" $params(t)]} {
    foreach line [shf_mthole_list] {
	lappend pin_line_list $line
    }
}

# Line with the main element parameters
if [info exists pin_line_list] {
    set element_line [element_line $pin_line_list]
} elseif [info exists pad_line_list] {
    set element_line [element_line $pad_line_list]
} else {
    puts "No list of pins or pads"
    exit
}

# Make the courtyard
set courtyard_line_list [list]
if {[string match "dda" $params(t)] || \
	[string match "dgk" $params(t)] || \
	[string match "cts625" $params(t)] || \
	[string match "dscdfn" $params(t)] || \
	[string match "pts645" $params(t)] || \
	[string match "dcu" $params(t)] || \
	[string match "dca" $params(t)]} {
    # Add the courtyard boundary
    set courtyard_line_list [dil_courtyard_lines $pad_line_list]
    # Add the inner assembly lines
    set assembly_line_list [dil_assembly_lines $pad_line_list]
} elseif {[string match "shf" $params(t)]} {
    set courtyard_line_list [shf_boundary_lines]
} elseif {[string match "snapspace" $params(t)]} {
    set courtyard_line_list [round_mthole_boundary_line $mthole(od_mm)]
} elseif {[string match "tsws" $params(t)]} {
    set courtyard_line_list [tsws_boundary_lines]
} elseif {[string match "lmzfl" $params(t)]} {
    set courtyard_line_list [lmzfl_boundary_lines]
} elseif {[string match "r78e" $params(t)]} {
    set courtyard_line_list [r78e_boundary_lines]
} elseif {[string match "mta100" $params(t)]} {
    set courtyard_line_list [mta100_boundary_lines]
} elseif {[string match "0603?" $params(t)] || \
	      [string match "1612b" $params(t)] || \
	      [string match "0805l" $params(t)] || \
	      [string match "4x4l" $params(t)] || \
	      [string match "8x8l" $params(t)] || \
	      [string match "1206?" $params(t)] || \
	      [string match "1210?" $params(t)]} {
    set courtyard_line_list [chip_courtyard_lines $pad_line_list none]
} elseif {[string match "ucl10x10" $params(t)]} {
    set courtyard_line_list [ucl10x10_boundary_lines]
} elseif {[string match "musbr" $params(t)]} {
    set courtyard_line_list [musbr_boundary_lines]
} elseif {[string match "eple" $params(t)]} {
    set courtyard_line_list [eple_boundary_lines]
}

# Alternate way to make the courtyard and assembly/boundary lines.
switch $params(t) {
    "db" -
    "dbv" {
	# Add the courtyard boundary and pin 1 dot
	set courtyard_line_list [dil_courtyard_lines $pad_line_list]
    }
    "anano" {
	# Arduino Nano
	set courtyard_line_list [anano_boundary_lines]
    }
    "astar_328pb" {
	# Pololu A-star 328pb
	set courtyard_line_list [astar_328pb_boundary_lines]
	set assembly_line_list [astar_328pb_assembly_lines]

    }
    "tswsra" {
	# Single-row right-angle unshrouded TSW header
	set courtyard_line_list [tswsra_boundary_lines]
    }
    "jstph" {
	# Single-row shrouded JST-PH header
	set courtyard_line_list [jstph_boundary_lines]
    }
    "xover_click" -
    "mp3_2_click" -
    "audioamp_2_click" {
	set courtyard_line_list [click_boundary_lines $click_length_mm]
    }
    "tst" {
	set courtyard_line_list [tst_boundary_lines]
    }
    "apt3216" {
	set courtyard_line_list [chip_courtyard_lines $pad_line_list]
	set assembly_line_list [diode_assembly_lines 2 1]
    }
    "smad" {
	# SMA diodes need special scaling for diode symbol
	set courtyard_line_list [chip_courtyard_lines $pad_line_list]
	set assembly_line_list [diode_assembly_lines 2.5 1]
    }
    "apt1608" {
	# 0603 LEDs need special scaling for diode symbol
	set courtyard_line_list [chip_courtyard_lines $pad_line_list]
	set assembly_line_list [diode_assembly_lines 1.7 1]
    }
    "smcw8" {
	# 0805 LEDs need special scaling for diode symbol
	set courtyard_line_list [chip_courtyard_lines $pad_line_list]
	set assembly_line_list [diode_assembly_lines 1.8 1]
    }
    "tstsm" {
	set courtyard_line_list [tstsm_boundary_lines]
	set assembly_line_list [tstsm_assembly_lines $pad_line_list]
    }
    "m7p" -
    "6-32p" {
	set courtyard_line_list [circle_courtyard_line $mthole(courtyard_mm)]
    }
    "dbz" -
    "sot323" {
	# Add the courtyard boundary
	set courtyard_line_list [dbz_courtyard_lines $pad_width_mm $body_length_mm $body_width_mm]
    }
    "d" {
	# set courtyard_line_list [dil_courtyard_lines $pad_line_list]
	set assembly_line_list [dil_assembly_lines $pad_line_list]
    }
    "smbth" {
	set courtyard_line_list [smbth_courtyard_lines]
    }
    "sj5" {
	set courtyard_line_list [sj5_courtyard_lines]
    }
    "3386f" {
	set courtyard_line_list [rectangle_boundary_lines $boundary_xlength_mm $boundary_ylength_mm]
    }
    "ces-571423" {
	set courtyard_line_list [polygon_boundary_lines [list -28.75 11.5 -28.75 -11.5 28.75 -11.5 28.75 11.5]]
	lappend courtyard_line_list {*}[polygon_boundary_lines [list -43 11.5 -43 -0.7 -28.75 -0.7 -28.75 11.5]]
	lappend courtyard_line_list {*}[polygon_boundary_lines [list 28.75 0.7 28.75 -11.5 43 -11.5 43 0.7]]
	
    }
}

# Write everything to the element list
lappend element_list $element_line
lappend element_list "("

# Alternate way to make the element list
if [info exists pin_line_list] {
    foreach pin $pin_line_list {
	lappend element_list $pin
    }
} elseif [info exists pad_line_list] {
    foreach pad $pad_line_list {
	lappend element_list $pad
    }
} else {
    puts "No list of pins or pads"
    exit
}

if [info exists slug_list] {
    foreach line $slug_list {
	lappend element_list $line
    }
}

if [info exists assembly_line_list] {
    foreach line $assembly_line_list {
	lappend element_list $line
    }
}

if {[string match "db" $params(t)] || \
	[string match "dgk" $params(t)] || \
	[string match "dcu" $params(t)] || \
	[string match "dca" $params(t)]} {
    foreach line $assembly_line_list {
	lappend element_list $line
    }
}

foreach line $courtyard_line_list {
    lappend element_list $line
}

lappend element_list ")"

# Write the file
write_lines $fp $element_list

puts "Wrote $params(o)"
close $fp
