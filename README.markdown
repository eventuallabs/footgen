# footgen -- Footprint generator for PCB #

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [footgen -- Footprint generator for PCB](#footgen----footprint-generator-for-pcb)
    - [Footprint types](#footprint-types)
        - [musbr](#musbr)
    - [Release history](#release-history)
        - [Version 1.0.0](#version-100)

<!-- markdown-toc end -->

## Footprint types ##

### musbr ###

![musbr_footprint][musbr_footprint]

| Manufacturer | Part number |
|:------------:|:-----------:|
| Amphenol ICC | MUSBRM1C130 |

This is the right-angle version of this PCB-mount USB-C connector.


[musbr_footprint]: media/thumbs/musbrm13130_thumb.png

## Release history ##

### Version 1.0.0 ###

